package com2008;

import DbManage.RegistrationManager;
import com2008.controllers.GenericController;
import com2008.routes.jbcrypt.BCrypt;
import com2008.controllers.Controller;
import com2008.controllers.RoleToController;
import com2008.routes.Route;
import com2008.ui.Generic;
import java.sql.SQLException;
import com2008.ui.View;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * Manager class, this will control a UI.
 */
public class Manager {

    //This should be our little secret
    private String controllerSrc;

    //The UI, username and routes all need to be public.
    public Generic ui;
    public String dbUsername;
    public String dbUrl;
    public String dbPass;
    public String username;
    public RegistrationManager db;

    public Route[] routes;
    public HashMap activities;
    //We need to define initial variable
    public Integer role = -1;
    public String currentView = null;
    public Controller c = null;

    /**
     * Constructor for Manager.
     */
    public Manager(String url, String username, String pass){

        this.dbUsername = username;
        this.dbUrl      = url;
        this.dbPass     = pass;

        initial();

        db = new RegistrationManager(url, username, pass);

        //Get which controller we're allowed.
        //Generic window creation.
        ui = new Generic("COM2008 | Registration System",this);

    }

    /**
     * Change the UI View given a new UI String, and the props
     * that may have.
     * @param newView the method string we need.
     * @param props Hashmap of properties we want.
     */
    public void changeView(String newView,HashMap props){
        System.out.println(props);
        //Create a new Route, create the view from the params.
        Route a = new Route();
        View v = a.getViewFromController(c,newView,props );

        //Set the buttons, content, and title to the needed stuffs.
        ui.setButtons(activities);
        ui.setContent(v.getSelf());
        ui.setTitleBar(v.getName());
        ui.setResizable(false);
        //ui.setBackground(new Color(138,188,209));
        currentView = newView;


        //Revalidate, and Repaint.
        ui.revalidate();
        ui.pack();
        ui.repaint();
    }
    public void onExit(){
        System.out.println("Exiting now.");
        try{
                this.db.conn.close();
            } catch (SQLException | NullPointerException e){
                System.out.println("Already closed or nothing to close.");
        }
        System.exit(0);

    }

    public void login(String username,String pass){

        if (db.checkPasswordEquality(username, pass)){
            controllerSrc = RoleToController.getMapping(db.getPrivilege(username,pass));

            System.out.println(db.getPrivilege(username,pass));
            System.out.println(controllerSrc);
            Manager self = this;
            this.username = username;

            try{
                //Get us a new controller.
                c = (Controller) Class.forName(controllerSrc).getConstructor(Manager.class).newInstance(self);
                System.out.println("Class has been found");
            }catch(ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    NoSuchMethodException  |
                    InvocationTargetException e){
                //Catch an invalid controller.
                System.out.println("Class not found");
            }

            activities = c.getActivities();
            HashMap d = new HashMap();
            d.put("id",username);
            ui.setFooterBar("Logged in as User with privilege: "+controllerSrc);
            changeView("displayMainMenu",d );



        }else{
            HashMap d = new HashMap();
            d.put("failure", true);
            changeView("login",d);

        }
    }
    public void changePassword(String username, String oldPass, String newPass){
        if (db.checkPasswordEquality(username, oldPass)) {
            int id = Integer.parseInt(db.executeQuery("SELECT id FROM User Where username="+username, new String[]{}).get(0).get("id"));
            db.generatePassword(newPass,id);
        }
    }
    public void initial(){
        //Set the controller for anonymous users.
        c = new GenericController(this);
        this.activities = null;
        //We need to set an example UI just to begin.
        currentView = "login";

    }
    public void logout(){
        HashMap d = new HashMap();
        d.put("loggedOut", true);
        ui.setFooterBar("Not Logged in");
        initial();

        changeView("login", d);
    }

    public static void main(String[] args) {

        //Define a new manager class.
        Manager p = new Manager("jdbc:mysql://stusql.dcs.shef.ac.uk/team011","team011","2fcf69f0");

        //Pack up the UI, and make it visible
        p.ui.init();
        p.ui.pack();
        p.ui.setVisible(true);

        //We can check if the user isn't logged in.



        //Change the view to something meaningful.
        p.changeView("login",new HashMap());

    }
}


