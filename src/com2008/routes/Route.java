package com2008.routes;

import com2008.controllers.Controller;
import com2008.ui.View;

import javax.naming.ldap.Control;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Route {


    /**
     * given a string, controller, and the parameters that view may take.
     * @param c Controller
     * @param desiredController
     * @param params
     * @return
     */
    public View getViewFromController(Controller c,String desiredController,HashMap params){
        try{
            Method method = c.getClass().getMethod(desiredController, new Class[] {HashMap.class});
            return (View) method.invoke(c, params); // 4 is the argument to pass to the method
        }

        catch( NoSuchMethodException | IllegalAccessException e){
            System.out.println("Error here");
            System.out.println(e);
            return null;
        }catch (InvocationTargetException e){
            System.out.println("Error Invocation");
            System.out.println(e);
            e.getCause().printStackTrace();
            e.getCause();
            return null;
        }

    }



}