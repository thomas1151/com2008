package com2008.controllers;
import DbManage.RegistrationManager;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.views.administrator.displayModules;
import com2008.ui.views.registrar.*;
import com2008.ui.ViewInterface;
import com2008.ui.views.registrar.checkCreditSum;
import com2008.ui.views.registrar.displayCreditSum;
import com2008.ui.views.registrar.mainMenu;
import com2008.ui.views.student.displayStudent;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistrarController extends GenericController implements Controller {
    public RegistrationManager db;
    public HashMap activities;
    public Manager manager;

	public RegistrarController(Manager mger) {
		super(mger);

        activities = new HashMap();

        //Define our menu items, and say what functions they correlate to.
		//These functions should require NO INPUT, as it is not programmed
		//to accept.
        activities.put("Menu","displayMainMenu");
        activities.put("Update Student Optional Modules","selectStudentModule");
        activities.put("Check Credits","checkCreditSum");
        activities.put("Update students","displayStudents");
        activities.put("Check a Student is Registered","checkRegistered");
        activities.put("Change Password","changePassword");

		activities.put("Logout","logout");

        //Define Mr. Manager for this controller.
        manager = mger;

        //Manager has the power to switch the UI, and so this needs to be passed to
		//Views where the ChangeViewListener (or equiv) is being used. If this is not
		//passed, the UI won't be able to update.
	}

	/**
	 * Getter for activities.
	 * @return activities
	 */
    public HashMap getActivities(){
        return activities;
    }


	/**
	 * Display the getStudentModule view which gets the student the registrar wants to see
	 * @params props - empty hash map
	 * @return view - getStudentModule view
	 */
	public ViewInterface selectStudentModule(HashMap props) {
        HashMap d = new HashMap();
        d.put("users",manager.db.executeQuery("SELECT * FROM Student LEFT JOIN User ON Student.user=User.id;",new String[]{}));
        ViewInterface view = new getStudentModule(manager,d);
        return view;
    }

	/**
	 * Display the displayStudentModules passing all the data for the optional modules the student is taking
	 * @params props - hash map with the student id for the student to display
	 * @return view - displayStudentModules view
	 */
    public ViewInterface viewModules(HashMap props) {
        Map<String,String> student = manager.db.executeQuery("SELECT s.*, u.forename, u.surname, u.username FROM User u LEFT JOIN Student s on s.user=u.id WHERE u.id=\""+props.get("id")+"\";",new String[]{}).get(0);
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT m.name as `module_name`, m.ref_code, md.id, md.core,  md.credits, md.taught, smd.initial_grade,smd.resit_grade \n" +
                "FROM StudentModuleDegree smd\n" +
                "INNER JOIN ModuleDegree md ON smd.module = md.id\n" +
                "INNER JOIN Module m ON md.module = m.id\n" +
                "WHERE smd.student =\""+student.get("id")+"\"", new String[]{});
        HashMap d = new HashMap();
        HashMap<Integer, Object> newProps = new HashMap<>();

		props.put("student",student);
		props.put("registrar",true);
        int rowCount = 0;
        for (Map<String,String> row : data ) {
            System.out.println(row);
            newProps.put(Integer.parseInt(row.get("id")),row);
            rowCount+=1;
        }
        props.put("data",newProps);

        ViewInterface view = new displayModules(manager, props ,"Showing modules for student","Below are all the modules associated with student "+student.get("forename")+" "+student.get("surname")+" ("+student.get("username")+"). Use the buttons to change which modules are associated with this user.");
        return view;
    }
//
//    public ViewInterface displayModules(HashMap props) {
//		ArrayList<Map<String,String>> n  = manager.db.executeQuery("SELECT id FROM User WHERE User.id = \""+((HashMap)props.get(-1)).get("id")+"\";",new String[]{});
//
//		ArrayList<Map<String,String>> data  = manager.db.executeQuery("SELECT * FROM ModuleDegree md " +
//				"LEFT JOIN StudentModuleDegree sm ON sm.module = md.module " +
//				"WHERE sm.student = "+n.get(0).get("id")+" AND md.core = 0;",new String[]{});
//
//		HashMap d = new HashMap();
//		ArrayList<String> id = new ArrayList<>(data.size());
//		ArrayList<String> degree = new ArrayList<>(data.size());
//		ArrayList<String> level = new ArrayList<>(data.size());
//		ArrayList<String> module = new ArrayList<>(data.size());
//		ArrayList<String> taught = new ArrayList<>(data.size());
//		ArrayList<String> core = new ArrayList<>(data.size());
//		ArrayList<String> credit = new ArrayList<>(data.size());
//		for (Map<String,String> hash : data){
//			id.add(hash.get("id"));
//			degree.add(hash.get("degree"));
//			level.add(hash.get("level"));
//			module.add(hash.get("module"));
//			taught.add(hash.get("taught"));
//			core.add(hash.get("core"));
//			credit.add(hash.get("credit"));
//		}
//		d.put("id",id);
//		d.put("degree",degree);
//		d.put("level",level);
//		d.put("module",module);
//		d.put("taught",taught);
//		d.put("core", core);
//		d.put("credit", credit);
//		ViewInterface view = new displayStudentModules(manager, d, props);
//        return(view);
//    }

	/**
	 * Display the addModule view which gets the id of the module to add
	 * @params props - hash map containing student id
	 * @return view - addModule view
	 */
    public ViewInterface displayAddModules(HashMap props) {

        System.out.println("DISPLAY ADD MODULES PROPS");
        props.put("modules",manager.db.executeQuery("SELECT md.id, md.level, md.taught, md.core, md.credits, m.name, m.ref_code FROM ModuleDegree md INNER JOIN Module m ON md.module=m.id WHERE degree=\""+((HashMap)props.get("student")).get("degree")+"\"",new String[]{}));
        System.out.println(props);

        ViewInterface view = new addModule(manager, props);
		return view;
	}

	/**
	 * Add the module as one the student is taking then show the displayModules view
	 * @params props - hash map with student id and the module id to be added
	 * @return view - displayModules view
	 */
	public ViewInterface addModuleToDatabase(HashMap props) {
        System.out.println("ADD MODULE props");

	    System.out.println(props);

		ArrayList<Map<String,String>> n  = manager.db.executeQuery("SELECT id FROM User WHERE User.username = \""+props.get("id")+"\";",new String[]{});

		manager.db.executeUpdate("INSERT INTO StudentModuleDegree (student,module,initial_grade,resit_grade)"
					+ "VALUES ("+props.get("id")+","+props.get("module")+",0,0);");

		//Create a new controller to properly call the displayModules controller
        ViewInterface view = new displayModules(manager, props,"Showing modules for student","Below are all the modules associated with student "+((HashMap)props.get("student")).get("forename")+" "+((HashMap)props.get("student")).get("surname")+" ("+((HashMap)props.get("student")).get("username")+"). Use the buttons to change which modules are associated with this user. Previous Module successfully added.");
		return view;
	}

	/**
	 * Display the removeModule view which gets the module id the registrar wants to remove
	 * @params props - hash map containing student id
	 * @return view - removeModule view
	 */
	public ViewInterface displayRemoveModules(HashMap props) {
	    System.out.println("DISPLAY MOVE MODULE PROPS");
	    System.out.println(props);

	    Map<String,String> student = (HashMap) props.get("student");
	    HashMap d = new HashMap();

	    d.put("modules",manager.db.executeQuery("SELECT m.name as `module_name`, m.id, m.ref_code, md.core, md.credits, d.name as `degree_name`, md.taught, smd.initial_grade,smd.resit_grade "
                +"FROM ModuleDegree md "
                +"INNER JOIN Module m ON md.module = m.id "
                +"INNER JOIN Degree d ON md.degree = d.id "
                +"INNER JOIN StudentModuleDegree  smd ON smd.module = md.id "
                +"WHERE smd.student =\""+student.get("id")+"\"", new String[]{}));

	    d.put("student",student);
	    System.out.println(d);
		ViewInterface view = new removeModule(manager, d);
		return view;
	}

	/**
	 * Remove the selected module and the display the displayModules view
	 * @params props - hash map containing student id and module id to be removed
	 * @return view - displayModules view
	 */
	public ViewInterface removeModule(HashMap props) {
		ArrayList<Map<String,String>> n  = manager.db.executeQuery("SELECT id FROM User WHERE User.username = \""+props.get("id")+"\";",new String[]{});

//		System.out.println(n.get(0).get("id") + "    " + props.get("module"));
        System.out.println("PROPS");
        System.out.println(props);
		manager.db.executeUpdate("DELETE FROM StudentModuleDegree WHERE student = "+((HashMap)props.get("student")).get("id")+" AND module = "+props.get("module")+";");
		//Create a controller to run displayModule properly
        ViewInterface view = new displayModules(manager, props,"Showing modules for student","Below are all the modules associated with student "+((HashMap)props.get("student")).get("forename")+" "+((HashMap)props.get("student")).get("surname")+" ("+((HashMap)props.get("student")).get("username")+"). Use the buttons to change which modules are associated with this user. Previous Module successfully deleted.");
		return view;
	}

	/**
	 * Display the addStudent view which gets the student information to add
	 * @params props - empty hash map
	 * @return view - addStudent view
	 */
	public ViewInterface addStudentForm(HashMap props) {
	    HashMap d = new HashMap();
	    d.put("roles",manager.db.executeQuery("Select * FROM Role WHERE name=\"Student\"",new String[]{}));
        ViewInterface view = new com2008.ui.views.administrator.addUserAccount(manager,d);
        return view;
    }

	public ViewInterface addUser(HashMap props) {
	    System.out.println("ADD USER HERE");
		if(props.get("role").equals("4")){
			String q = "INSERT INTO User (role,title,forename,surname,email,username) VALUES "
					+ " (\""+props.get("role")+"\",\""+props.get("title")+"\",\""+props.get("forename")+"\",\""+props.get("surname")+"\",\""+props.get("email")+"\",\""+props.get("username")+"\");";
			System.out.println(q);
			manager.db.executeUpdate(q);
			q = "SELECT id FROM User WHERE surname=\""+props.get("surname")+"\""+" AND email=\""+props.get("email")+"\" ORDER BY ID DESC LIMIT 1 ";
			System.out.println(q);
            props.put("user",manager.db.executeQuery(q, new String[]{}).get(0).get("id"));
			manager.db.generatePassword((String) props.get("username"), Integer.parseInt((String) props.get("user")));

            q = "SELECT * FROM Degree ";
            props.put("degrees",manager.db.executeQuery(q, new String[]{}));

            q = "SELECT * FROM User ";
            props.put("students",manager.db.executeQuery(q, new String[]{}));

			return new com2008.ui.views.administrator.addStudentAccount(manager,props);

		}else{
			props.put("error","Must be a student");
			return displayStudents(props);
		}
	}

	/**
	 * Add the student to the database and then run the displayStudent view
	 * @params props - hash map containing the id, degree, title, forename, surname, email, tutor, password
	 * @return view - displayStudents view
	 */
    public  ViewInterface addStudent(HashMap props) {

    	String q = "INSERT INTO Student (user, degree, tutor) VALUES (\""+props.get("user")+"\",\""+props.get("degree")+"\",\""+props.get("tutor")+"\");";
    	System.out.println(q);
    	manager.db.executeUpdate(q);

    	String studyPeriodId;
    	if(props.get("studyperiod") == null){
    	    studyPeriodId = manager.db.executeQuery("SELECT * " +
                    "FROM StudyPeriod " +
                    "ORDER BY ABS( DATEDIFF( `start`, NOW() ) ) " +
                    "LIMIT 1", new String[]{}).get(0).get("id");
        }else{
    	    studyPeriodId = (String) props.get("studyperiod");
        }

    	String studentID = manager.db.executeQuery("SELECT id FROM Student WHERE tutor=\""+props.get("tutor")+"\"ORDER BY ID DESC LIMIT 1", new String[]{}).get(0).get("id");
		manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (student,studyPeriod,level,code,overall_grade)"
				+ "VALUES ("+studentID+","+studyPeriodId+", 1,\"A\",0);");


		ArrayList<Map<String,String>> l = manager.db.executeQuery("SELECT * FROM ModuleDegree WHERE degree = " + props.get("degree") + " AND core = 1 AND level = 1", new String[]{});

		for (Map<String,String> hash : l){
			manager.db.executeUpdate("INSERT INTO StudentModuleDegree (module, student,initial_grade,resit_grade)"
					+ "VALUES (" + hash.get("id") + "," + studentID + ",0,0);");
		}
		//Create a new controller to run displayStudents properly
		RegistrarController c = new RegistrarController(this.manager);
		ViewInterface view = c.displayStudents(props);
		return view;
    }

	/**
	 * Get the credit count for the selected student then display the displayCreditSum view
	 * @params props - hash map containing student id
	 * @return view - displayCreditSum view
	 */
	public ViewInterface sumCredits(HashMap props) {
		System.out.println("SUM CREDIT PROPS");
		System.out.println(props);
		ArrayList<Map<String, String>> data = manager.db.executeQuery("SELECT SUM(md.credits) as \"credits\" FROM " +
                " ModuleDegree md" +
                " LEFT JOIN StudentModuleDegree smd ON smd.module = md.id" +
                " WHERE smd.student=\"" + props.get("id") + "\";", new String[]{});
		View view = new displayCreditSum(manager, data);
		return view;
	}

	/**
	 * Display the checkCreditSum view which gets the student the registrar wants to check the credit sum of
	 * @params props - empty hash map
	 * @return view - checkCreditSum view
	 */
	public ViewInterface checkCreditSum(HashMap props){
        HashMap d = new HashMap();
        d.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));
		ViewInterface view = new checkCreditSum(manager, d);
		return view;
	}


	/**
	 * Display the MainMenu for our registrar controller.
	 * @param props has to take props, even if we don't use them.
	 * @return the view that corresponds to us viewing the MainMenu.
	 */
	public View displayMainMenu(HashMap props) {
		View view = new mainMenu(manager);

		return view;
	}

	/**
	 * Display the checkRegistered view which gets the student the registrar wants to check is registered
	 * @params props - empty hash map
	 * @return view - checkRegistered view
	 */
	public ViewInterface checkRegistered(HashMap props) {
	    HashMap d = new HashMap();
		d.put("users",manager.db.executeQuery("SELECT * FROM Student LEFT JOIN User ON Student.user=User.id;",new String[]{}));
        ViewInterface view = new checkRegistered(manager,d);
		return view;
	}

	/**
	 * Display the removeStudent view which gets the student the registrar wants to remove
	 * @params props - empty hash map
	 * @return view - removeStudent view
	 */
	public ViewInterface selectStudentToRemove(HashMap props) {
	    if(props==null){
	        props = new HashMap();
        }
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));

        ViewInterface view = new removeStudent(manager,props);
        return view;
    }

	/**
	 * Remove the student from the database then display the displayStudents view
	 * @params props - hash map with student id
	 * @return view - displayStudents view
	 */
    public ViewInterface removeStudent(HashMap props) {
        System.out.println(props);
        String studentId = manager.db.executeQuery("SELECT * FROM Student WHERE id=\""+props.get("id")+"\";", new String[]{}).get(0).get("id");

		manager.db.executeUpdate("DELETE FROM"
				+ " StudentModuleDegree WHERE student="+props.get("id")+";");

		manager.db.executeUpdate("DELETE FROM"
				+ " StudentStudyPeriod WHERE student ="+props.get("id")+";");

        manager.db.executeUpdate("DELETE FROM"
                + " Student WHERE id="+props.get("id")+";");

		manager.db.executeUpdate("DELETE FROM"
				+ " User WHERE id =\""+studentId +"\";");


        ViewInterface view = displayStudents(props);
		return view;
    }

	/**
	 * Get all the students from the database and pass them to the displayStudents view and then display this view
	 * @params props - empty hash map
	 * @return view - displayStudents view
	 */
	public ViewInterface displayStudents(HashMap props) {
		 ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT DISTINCT s.id, u.title, u.forename,u.surname,u.username,u.email, d.name,d.yii,d.max_level, s.tutor, ssp.code,ssp.overall_grade,ssp.`level`" +
                 " FROM Student s" +
                 " INNER JOIN User u ON s.user=u.id" +
                 " INNER JOIN StudentStudyPeriod ssp ON s.id = ssp.student" +
                 " INNER JOIN StudentModuleDegree smd ON smd.student =  s.id" +
                 " INNER JOIN ModuleDegree md ON smd.module = md.id" +
                 " INNER JOIN `Degree` d ON md.`degree` = d.id",new String[]{});

		 if(props == null) {
             props = new HashMap();
         }
        HashMap<Integer, Object> newProps = new HashMap<>();
        int rowCount = 0;
        for (Map<String,String> row : data ) {
            System.out.println(row);

            newProps.put(Integer.parseInt(row.get("id")),row);
            rowCount+=1;
        }
        props.put("data",newProps);
        System.out.println(props);
        ViewInterface view = new displayStudents(manager, props);

		return view;
	}

	/**
	 * Show the CheckRegistration options
	 * @param props we definitely require id in the props
	 * @return the view that corresponds to us viewing the MainMenu.
	 */
	public ViewInterface checkRegistration(HashMap props) {
        if(props == null){
            props = new HashMap();
        }


		ArrayList<Map<String, String>> d = new ArrayList<>();



		//

		Map<String,String> userDetails = manager.db.executeQuery("SELECT  u.*,s.id as \"student_id\" ,s.tutor, d.name as \"degree_name\", ssp.code as \"study_code\", l.name as \"level_name\", r.name as \"role\" FROM Student s " +
				"LEFT JOIN User u ON s.user=u.id " +
				"INNER  JOIN Role r ON u.`role` = r.id " +
				"LEFT JOIN Degree d ON s.degree = d.id "+
				"LEFT JOIN StudentStudyPeriod ssp ON  ssp.id = (SELECT ssp.id FROM StudentStudyPeriod ssp  WHERE  ssp.student = s.id ORDER BY id DESC LIMIT 1) " +
				"LEFT JOIN `Level` l ON ssp.`level` = l.code"+
				" WHERE" +
				" u.id = \""+props.get("id")+"\";",new String[]{}).get(0);

		props.put("userDetails",userDetails);

		String q  = "SELECT smd.id, smd.initial_grade, smd.resit_grade, md.core, md.credits, m.name as \"module_name\", m.ref_code,l.code as \"level_code\", l.name as \"level_name\" " +
				"FROM StudentModuleDegree smd " +
				"LEFT JOIN ModuleDegree md ON smd.module = md.id " +
				"LEFT JOIN Module m ON md.module = m.id " +
				"LEFT JOIN Level l ON  md.`level` = l.code " +
				"LEFT JOIN Student s ON smd.student = s.id " +
				"LEFT JOIN User u ON s.user = u.id " +
				"WHERE u.id = \""+props.get("id")+"\";";
		ArrayList<Map<String,String>> moduleInformation = manager.db.executeQuery(q,new String[]{});
		HashMap<Integer, Object> newProps = new HashMap<>();
        System.out.println("moduleInformation!");
		System.out.println(moduleInformation);

		for (Map<String,String> module : moduleInformation ) {

			newProps.put(Integer.parseInt(module.get("id")),module);

		}

		props.put("moduleDetails",newProps);

		HashMap levels = new HashMap();

		for (Map<String,String> module : moduleInformation) {
			levels.putIfAbsent(module.get("level_code"), 0);
			levels.putIfAbsent(module.get("level_code")+"_possible", 0);

			levels.put(module.get("level_code")+"_possible",  (Integer) levels.get(module.get("level_code")+"_possible") + Integer.parseInt(module.get("credits")));

			Integer initialGrade  = Integer.parseInt(module.get("initial_grade"));
			Integer resitGrade = Integer.parseInt(module.get("resit_grade"));
			Integer actualGrade = 0;
			if( (initialGrade < 40) && (resitGrade > initialGrade)){
				actualGrade = resitGrade;
				if(resitGrade > 40){
					actualGrade = 40;
				}
			}else{
				actualGrade = initialGrade;
			}
			Integer credits;
			if(actualGrade >= 40){
				credits = Integer.parseInt(module.get("credits"));
			}
			else{
				credits = 0;
			}
			levels.put( (String) module.get("level_code"), (Integer) (levels.get(module.get("level_code"))) + credits );
		}
		props.put("levels",levels);
        System.out.println(levels);
        props.put("done", "false");
        if(levels.size() > 0) {
            for (Object key : levels.keySet()) {
                String ourKey = (String) key;
                if(ourKey.contains("_possible")){
                    continue;
                }

                if ((Integer) levels.get( (ourKey + "_possible")) >= 120) {
                    props.put("done", "true");
                }else{
                    props.put("done", "false");
                }
            }
            props.put("registered","true");
        }else{
            props.put("registered","false");
        }

		ViewInterface view = new displayRegistered(manager, props);

		return view;
	}

}