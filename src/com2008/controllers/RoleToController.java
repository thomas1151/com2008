package com2008.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RoleToController {
    private static final Map<Integer, String> mapping = initMap();

    /**
     * the mapping from roles to controllers
     * @return the Map
     */
    private static Map<Integer, String> initMap() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "com2008.controllers.AdministratorController");
        map.put(2, "com2008.controllers.RegistrarController");
        map.put(3, "com2008.controllers.TeacherController");
        map.put(4, "com2008.controllers.StudentController");
        return Collections.unmodifiableMap(map);
    }

    /**
     * Get the controller responsible for the role ID given
     * @param i
     * @return the controller string we need.
     */
    public static String getMapping(Integer i){
        return initMap().get(i);
    }
}
