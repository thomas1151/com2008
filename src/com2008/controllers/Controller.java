package com2008.controllers;
import com2008.Manager;
import java.util.HashMap;


/**
 *  Interface for our controllers.
 *  These must define a manager, and the activities the controller prohibits.
 */
public interface Controller {
    Manager manager  = null;
    HashMap activities = new HashMap();
    default Manager getManager(){return manager;}
    default HashMap getActivities(){return activities;}
}
