package com2008.controllers;
import DbManage.RegistrationManager;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.views.registrar.displayRegistered;
import com2008.ui.views.student.displayStudent;
import com2008.ui.ViewInterface;


import java.sql.SQLClientInfoException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentController extends GenericController implements Controller {
    public RegistrationManager db;
    public HashMap activities;
    public Manager manager;

    public StudentController(Manager mger) {
        super(mger);

        activities = new HashMap();

        //Define our menu items, and say what functions they correlate to.
        //These functions should require NO INPUT, as it is not programmed
        //to accept.
        activities.put("Logout","logout");
        activities.put("View My details","displayDetails");
        activities.put("Change Password","changePassword");


        //Define Mr. Manager for this controller.
        manager = mger;

        //Manager has the power to switch the UI, and so this needs to be passed to
        //Views where the ChangeViewListener (or equiv) is being used. If this is not
        //passed, the UI won't be able to update.
    }

    /**
     * Getter for activities.
     * @return activities
     */
    public HashMap getActivities(){
        return activities;
    }


    /**
     * Display the getStudentModule view which gets the student the registrar wants to see
     * @params props - empty hash map
     * @return view - getStudentModule view
     */

    public ViewInterface  displayMainMenu(HashMap props) {
       return displayDetails(props);
    }

    public ViewInterface displayDetails(HashMap props){
        if(props == null){
            props = new HashMap();
        }
        Map<String,String> data = manager.db.executeQuery("SELECT  u.*,s.id as \"student_id\" ,s.tutor, d.name as \"degree_name\", ssp.code as \"study_code\", l.name as \"level_name\", r.name as \"role\" FROM Student s " +
                "LEFT JOIN User u ON s.user=u.id " +
                "INNER  JOIN Role r ON u.`role` = r.id " +
                "LEFT JOIN Degree d ON s.degree = d.id "+
                "LEFT JOIN StudentStudyPeriod ssp ON  ssp.id = (SELECT ssp.id FROM StudentStudyPeriod ssp  WHERE  ssp.student = s.id ORDER BY id DESC LIMIT 1) " +
                "LEFT JOIN `Level` l ON ssp.`level` = l.code"+
                " WHERE" +
                " u.username = \""+manager.username+"\";",new String[]{}).get(0);

        props.put("userDetails",data);

        String q  = "SELECT smd.id, smd.initial_grade, smd.resit_grade, md.core, md.credits, m.name as \"module_name\", m.ref_code,l.code as \"level_code\", l.name as \"level_name\" " +
                "FROM StudentModuleDegree smd " +
                "LEFT JOIN ModuleDegree md ON smd.module = md.id " +
                "LEFT JOIN Module m ON md.module = m.id " +
                "LEFT JOIN Level l ON  md.`level` = l.code " +
                "LEFT JOIN Student s ON smd.student = s.id " +
                "LEFT JOIN User u ON s.user = u.id " +
                "WHERE u.username = \""+manager.username+"\";";
        ArrayList<Map<String,String>> moduleInformation = manager.db.executeQuery(q,new String[]{});
        HashMap<Integer, Object> newProps = new HashMap<>();
        int rowCount = 0;
        for (Map<String,String> row : moduleInformation ) {
            System.out.println(row);
            newProps.put(Integer.parseInt(row.get("id")),row);
            rowCount+=1;
        }

        props.put("moduleDetails",newProps);

        HashMap levels = new HashMap();

        for (Map<String,String> module : moduleInformation) {
            levels.putIfAbsent(module.get("level_code"), 0);
            levels.putIfAbsent(module.get("level_code")+"_possible", 0);

            levels.put(module.get("level_code")+"_possible",  (Integer) levels.get(module.get("level_code")+"_possible") + Integer.parseInt(module.get("credits")));

            Integer initialGrade  = Integer.parseInt(module.get("initial_grade"));
            Integer resitGrade = Integer.parseInt(module.get("resit_grade"));
            Integer actualGrade = 0;
            if( (initialGrade < 40) && (resitGrade > initialGrade)){
                actualGrade = resitGrade;
                if(resitGrade > 40){
                    actualGrade = 40;
                }
            }else{
                actualGrade = initialGrade;
            }
            Integer credits;
            if(actualGrade >= 40){
                credits = Integer.parseInt(module.get("credits"));
            }
            else{
                credits = 0;
            }
            levels.put( (String) module.get("level_code"), (Integer) (levels.get(module.get("level_code"))) + credits );
        }
        props.put("levels",levels);


        ViewInterface view = new displayStudent(manager, props);
        return view;

    }
}