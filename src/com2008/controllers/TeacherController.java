package com2008.controllers;
import DbManage.RegistrationManager;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.views.administrator.displayDegreeCourses;
import com2008.ui.views.administrator.mainMenu;
import com2008.ui.views.registrar.displayResitGrades;
import com2008.ui.views.registrar.displayStudents;
import com2008.ui.views.registrar.removeStudent;
import com2008.ui.views.teacher.displayStudentMean;
import com2008.ui.views.teacher.*;
import com2008.ui.ViewInterface;
import org.omg.PortableInterceptor.INACTIVE;


import java.sql.SQLClientInfoException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeacherController extends GenericController implements Controller {
    public RegistrationManager db;
    public HashMap activities;
    public Manager manager;

    public TeacherController(Manager mger) {
        super(mger);

        activities = new HashMap();

        //Define our menu items, and say what functions they correlate to.
        //These functions should require NO INPUT, as it is not programmed
        //to accept.
        activities.put("Add/Update Grades","selectStudentUpdate");
        activities.put("See Student Details","displayStudents");
        activities.put("Calculate Degree Result","displayDegreeResult");
        activities.put("Calculate Student Mean","selectStudentMean");
        activities.put("Logout","logout");
        activities.put("Change Password","changePassword");

        //Define Mr. Manager for this controller.
        manager = mger;

        //Manager has the power to switch the UI, and so this needs to be passed to
        //Views where the ChangeViewListener (or equiv) is being used. If this is not
        //passed, the UI won't be able to update.
    }

    /**
     * Getter for activities.
     * @return activities
     */
    public HashMap getActivities(){
        return activities;
    }


    /**
     * Show the displayStudentDetails view passing it details on all the students
     * @params props - empty hash map
     * @return view - displayStudentDetails view
     */
	public ViewInterface  getStudentInfo(HashMap props) {
		ArrayList<Map<String,String>> result = db.executeQuery("SELECT * FROM StudentModule", new String[]{} );

		ViewInterface view = new displayStudentDetails(manager, props);
		return view;

	}

    /**
     * Update the database with the new grades then display displayUpdateGrades view
     * @params props - hash map with the student id and module id
     * @return view - displayUpdateGrades view
     */
	public ViewInterface updateGrades(HashMap props) {
	    System.out.println("UPDATE GRADES");
	    System.out.println(props);
	    String q = "UPDATE StudentModuleDegree "
                + "SET initial_grade="+props.get("grade")
                +" WHERE id="+props.get("module")+";";
	    System.out.println(q);
        manager.db.executeUpdate(q);


        ViewInterface view = selectStudentUpdate(props);
		return view;
	}

    /**
     * Show the displayStudentDetails view passing it details on all the students
     * @params props - empty hash map
     * @return view - displayStudentDetails view
     */
    public ViewInterface displayUpdateGrades(HashMap props) {
        System.out.println("DISPLAY UPDATE GRADE PROPS");
        System.out.println(props);
        if(props == null){
            props = new HashMap();
        }else{
            props.put("modules",manager.db.executeQuery("SELECT m.ref_code, smd.resit_grade, smd.initial_grade, smd.id , m.name " +
                    "FROM StudentModuleDegree smd " +
                    "LEFT JOIN ModuleDegree md ON smd.module = md.id " +
                    "LEFT JOIN Module m ON md.module = m.id " +
                    "WHERE " +
                    "smd.student = "+props.get("id"),new String[]{}));
        }

        ViewInterface view = new displayUpdateGrades(manager, props);
	    return view;
    }

    /**
     * Show the selectStudentUpdate view
     * @params props - empty hash map
     * @return view - selectStudentUpdate view
     */

    public ViewInterface selectStudentUpdate(HashMap props) {
        if(props == null){
            props = new HashMap();
        }

        ArrayList<Map<String,String>> data =manager.db.executeQuery("SELECT m.ref_code, m.name, smd.initial_grade, smd.resit_grade, u.forename, u.surname, smd.id, s.id  as \"student_id\" FROM" +
                " StudentModuleDegree smd" +
                " LEFT JOIN Student s ON s.id = smd.student" +
                " LEFT JOIN ModuleDegree md ON smd.module = md.id"+
                " LEFT JOIN Module m on md.module = m.id" +
                " LEFT JOIN User u ON u.id = s.user ", new String[]{});

        HashMap<Integer, Object> newProps = new HashMap<>();
        for (Map<String,String> row : data ) {
            newProps.put(Integer.parseInt(row.get("id")),row);
        }

        props.put("studentGrades",newProps);
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));
        ViewInterface view = new selectStudentUpdate(manager, props);
        return view;
    }

    /**
     * Update the StudentModuleDegree table with the new resit grades
     * @params props - empty hash map
     * @return view - selectStudentUpdate view
     */
	public ViewInterface updateResitGrades(HashMap props) {


        manager.db.executeUpdate("UPDATE StudentModuleDegree "
				+ "SET resit_grade="+props.get("grade")
				+" WHERE id="+props.get("module"));

        ViewInterface view = selectStudentUpdate(props);
        return view;
	}

    /**
     * Display the displayResitGrades view
     * @params props - empty hash map
     * @return view - displayResitGrades view
     */
	public ViewInterface displayUpdateResit(HashMap props) {
        System.out.println("DISPLAY UPDATE RESIT PROPS");
        System.out.println(props);
        if(props == null){
            props = new HashMap();
        }else{
            props.put("modules",manager.db.executeQuery("SELECT m.ref_code, smd.resit_grade, smd.initial_grade, smd.id , m.name " +
                    "FROM StudentModuleDegree smd " +
                    "LEFT JOIN ModuleDegree md ON smd.module = md.id " +
                    "LEFT JOIN Module m ON md.module = m.id " +
                    "WHERE " +
                    "smd.student =\""+props.get("id")+"\";",new String[]{}));
        }


        ViewInterface view = new displayResitGrades(manager, props);
        return view;
    }

    /**
     * Works out and gives the selected students mean to the displayStudentMean view displaying this
     * @params props - hash map with the student id
     * @return view - displayStudentMean view
     */
	public ViewInterface calculateMean(HashMap props) {
        System.out.println("CALCULCATE MEAN");
        System.out.println(props);
		String[] cols = new String[]{"module", "initialGrade","resitGrade"};

        ArrayList<Map<String,String>> r = manager.db.executeQuery("SELECT md.credits, smd.initial_grade, smd.resit_grade, md.level FROM" +
                " StudentModuleDegree smd" +
                " LEFT JOIN ModuleDegree md ON smd.module = md.id" +
                " WHERE" +
                " md.`level` > 1 AND" +
                " smd.student ="+props.get("id")+";", new String[]{});

        Map<Integer,Double> levels = new HashMap<>();

        for (int x=0; x < r.size();x++) {
            Integer totalCredits = 120;
            if(r.get(x).get("credits").equals("5")){
                totalCredits = 150;
            }
            char temp = r.get(x).get("level").charAt(0);
            if(Character.isDigit(temp)) {
                double weight = (double)Integer.parseInt(r.get(x).get("credits"))/ totalCredits;
                int score  = calculateWhichGradeToUse(Integer.parseInt(r.get(x).get("initial_grade")),Integer.parseInt(r.get(x).get("resit_grade")));

                levels.putIfAbsent(Integer.parseInt(r.get(x).get("level")), 0.0);
                levels.put(Integer.parseInt(r.get(x).get("level")), levels.get(Integer.parseInt(r.get(x).get("level"))) + (weight*score));

            }

        }
        int numberOfLevels = levels.size();
        double val = 0;
        int    currentLevel = 0;
        if(numberOfLevels > 0) {
            for (Map.Entry<Integer, Double> entry : levels.entrySet()) {
                Integer key = entry.getKey();
                Double value = entry.getValue();
                if(currentLevel < key){
                    val = value;
                }
            }
        }
        props.put("mean", val);
        props.put("student", manager.db.executeQuery("SELECT  u.*,s.id as \"student_id\" ,s.tutor, d.name as \"degree_name\", ssp.code as \"study_code\", l.name as \"level_name\", r.name as \"role\" FROM Student s " +
                "LEFT JOIN User u ON s.user=u.id " +
                "INNER  JOIN Role r ON u.`role` = r.id " +
                "LEFT JOIN Degree d ON s.degree = d.id "+
                "LEFT JOIN StudentStudyPeriod ssp ON  ssp.id = (SELECT ssp.id FROM StudentStudyPeriod ssp  WHERE  ssp.student = s.id ORDER BY id DESC LIMIT 1) " +
                "LEFT JOIN `Level` l ON ssp.`level` = l.code"+
                " WHERE" +
                " s.id = \""+props.get("id")+"\";",new String[]{}).get(0));
		ViewInterface view = new displayStudentMean(manager, props);
        return view;
	}

    /**
     * Display the selectedStudentMean view
     * @params props - empty hash map
     * @return view - selectStudentMean view
     */
	public ViewInterface selectStudentMean(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));
	    ViewInterface view = new selectStudentMean(manager,props);
	    return view;
    }

    /**
     * Display the removeStudent view which gets the student the registrar wants to remove
     * @params props - empty hash map
     * @return view - removeStudent view
     */
    public ViewInterface selectStudentToRemove(HashMap props) {
        if(props==null){
            props = new HashMap();
        }
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));

        ViewInterface view = new removeStudent(manager,props);
        return view;
    }

    public Integer calculateWhichGradeToUse(Integer initialGrade, Integer resitGrade){
        if (initialGrade >resitGrade) {
            return initialGrade;
        }
        else {
            if(resitGrade >= 40) {
                return 40;
            }
            else {
                return resitGrade;
            }
        }
    }


    /**
     * Progress the student to the next level and display the selectStudentMean view
     * @params props - hash map with the id
     * @return view - selectStudentMean view
     */
    public ViewInterface progressStudent(HashMap props) {
        Map<String,String> data = manager.db.executeQuery("SELECT ssp.id, ssp.studyperiod, ssp.level, ssp.code, d.max_level " +
                " FROM StudentStudyPeriod ssp\n" +
                " LEFT JOIN Student s ON ssp.student = s.id\n" +
                " LEFT JOIN Degree d ON s.`degree` = d.id\n" +
                " WHERE student ="+props.get("id")+"  ORDER BY id DESC", new String[]{}).get(0);


        String v;
        if (data.get("level") == "P") {
            v = data.get("max_level");
        }
        else if (Integer.parseInt(data.get("level")) == (Integer.parseInt(data.get("max_level"))-1)) {
            v = "P";
        }
        else {
            v = Integer.toString(Integer.parseInt(data.get("level")) + 1);
        }

        int charValue = data.get("code").charAt(0);
        String x = String.valueOf( (char) (charValue + 1));

        Integer nextStudyPeriod = Integer.parseInt(data.get("studyperiod"))+1;

        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (studyPeriod,student,level,code) VALUES ("+nextStudyPeriod+","+props.get("id")+",\""+v+"\","+x+");" );


        ViewInterface view = selectStudentMean(props);
        return view;
    }

    /**
     * Graduate the student and then display the selectStudentMean view
     * @params props - hash map with the id
     * @return view - selectStudentMean view
     */
    public ViewInterface graduateStudent(HashMap props) {
        Map<String,String> data = manager.db.executeQuery("SELECT id, studyperiod, level, code" +
                " FROM StudentStudyPeriod" +
                " WHERE student ="+props.get("id")+"  ORDER BY id DESC", new String[]{}).get(0);

        Integer nextStudyPeriod = Integer.parseInt(data.get("studyperiod"))+1;

        int charValue = data.get("code").charAt(0);
        String x = String.valueOf( (char) (charValue + 1));

        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (studyPeriod,student,level,code) VALUES ("+nextStudyPeriod+","+props.get("id")+","+"\"G\""+","+x+");" );

        ViewInterface view = selectStudentMean(props);
        return view;
    }

    /**
     * Set the student to repeat the year then display the selectStudentMean view
     * @params props - hash map with the id
     * @return view - selectStudentMean view
     */
    public ViewInterface repeatStudent(HashMap props) {
        Map<String,String> data = manager.db.executeQuery("SELECT id, studyperiod, level, code" +
                " FROM StudentStudyPeriod" +
                " WHERE student ="+props.get("id")+"  ORDER BY id DESC", new String[]{}).get(0);


        Integer nextStudyPeriod = Integer.parseInt(data.get("studyperiod"))+1;

        int charValue = data.get("code").charAt(0);
        String x = String.valueOf( (char) (charValue + 1));

        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (studyPeriod,student,level,code) VALUES ("+nextStudyPeriod+","+props.get("id")+",\""+data.get("level")+"\","+x+");" );


        ViewInterface view = selectStudentMean(props);
        return view;
    }

    /**
     * Set the student to fail the year then display the selectStudentMean view
     * @params props - hash map with the id
     * @return view - selectStudentMean view
     */
    public ViewInterface failStudent(HashMap props) {

        Map<String,String> data = manager.db.executeQuery("SELECT id, studyperiod, level, code" +
                " FROM StudentStudyPeriod" +
                " WHERE student ="+props.get("id")+"  ORDER BY id DESC", new String[]{}).get(0);


        Integer nextStudyPeriod = Integer.parseInt(data.get("studyperiod"))+1;

        int charValue = data.get("code").charAt(0);
        String x = String.valueOf( (char) (charValue + 1));

        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (studyPeriod,student,level,code) VALUES ("+nextStudyPeriod+","+props.get("id")+","+"\"F\""+","+x+");" );


        TeacherController c = new TeacherController(this.manager);
        ViewInterface view = c.selectStudentMean(props);
        return view;
    }

    /**
     * Display the selectStudentMean view
     * @params props - empty hash map
     * @return view - selectDegreeResult view
     */
    public ViewInterface displayDegreeResult(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;",new String[]{}));
        ViewInterface view = new selectDegreeResult(manager,props);
	    return view;
    }

    public ViewInterface displayMainMenu(HashMap props){
        ViewInterface view  = new mainMenu(manager);
        return view;
    }
    /**
     * Calculates the students grade and then pass the data to the displayDegreeResult view
     * @params props - hash map with the student id
     * @return view - displayDegreeResult view
     */
	public ViewInterface calculateGrade(HashMap props) {
	    System.out.println("CALCULATE GRDADE");
	    System.out.println(props);
        ArrayList<Map<String,String>> r = manager.db.executeQuery("SELECT md.credits, smd.initial_grade, smd.resit_grade, md.level FROM" +
                " StudentModuleDegree smd" +
                " LEFT JOIN ModuleDegree md ON smd.module = md.id" +
                " WHERE" +
                " md.`level` > 1 AND" +
                " smd.student ="+props.get("id")+";", new String[]{});

        Map<Integer,Double> levels = new HashMap<>();

		for (int x=0; x < r.size();x++) {
            Integer totalCredits = 120;
            if(r.get(x).get("credits").equals("5")){
                totalCredits = 150;
            }
            char temp = r.get(x).get("level").charAt(0);
            if(Character.isDigit(temp)) {
                System.out.println(r.get(x));
                double weight = (double) Integer.parseInt(r.get(x).get("credits")) / totalCredits;
                int score = calculateWhichGradeToUse(Integer.parseInt(r.get(x).get("initial_grade")), Integer.parseInt(r.get(x).get("resit_grade")));

                levels.putIfAbsent(Integer.parseInt(r.get(x).get("level")), 0.0);
                levels.put(Integer.parseInt(r.get(x).get("level")), levels.get(Integer.parseInt(r.get(x).get("level"))) + (weight * score));
            }
		}
        System.out.println("levels");
		System.out.println(levels);

		if(props==null){
		    props = new HashMap();
        }
        props.put("levels",levels);

        ViewInterface view = new displayDegreeResult(manager, props);
		return view;
	}

    /**
     * Add the student to the database and then run the displayStudent view
     * @params props - hash map containing the id, degree, title, forename, surname, email, tutor, password
     * @return view - displayStudents view
     */
    public  ViewInterface addStudent(HashMap props) {

        String q = "INSERT INTO Student (user, degree, tutor) VALUES (\""+props.get("user")+"\",\""+props.get("degree")+"\",\""+props.get("tutor")+"\");";
        System.out.println(q);
        manager.db.executeUpdate(q);

        String studyPeriodId;
        if(props.get("studyperiod") == null){
            studyPeriodId = manager.db.executeQuery("SELECT * " +
                    "FROM StudyPeriod " +
                    "ORDER BY ABS( DATEDIFF( `start`, NOW() ) ) " +
                    "LIMIT 1", new String[]{}).get(0).get("id");
        }else{
            studyPeriodId = (String) props.get("studyperiod");
        }

        String studentID = manager.db.executeQuery("SELECT id FROM Student WHERE tutor=\""+props.get("tutor")+"\"ORDER BY ID DESC LIMIT 1", new String[]{}).get(0).get("id");
        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (student,studyPeriod,level,code,overall_grade)"
                + "VALUES ("+studentID+","+studyPeriodId+", 1,\"A\",0);");


        ArrayList<Map<String,String>> l = manager.db.executeQuery("SELECT * FROM ModuleDegree WHERE degree = " + props.get("degree") + " AND core = 1", new String[]{});

        for (Map<String,String> hash : l){
            manager.db.executeUpdate("INSERT INTO StudentModuleDegree (module, student,initial_grade,resit_grade)"
                    + "VALUES (" + hash.get("id") + "," + studentID + ",0,0);");
        }
        //Create a new controller to run displayStudents properly
        RegistrarController c = new RegistrarController(this.manager);
        ViewInterface view = c.displayStudents(props);
        return view;
    }



    public ViewInterface addUser(HashMap props) {
        System.out.println("ADD USER HERE");
        if(props.get("role").equals("4")){
            String q = "INSERT INTO User (role,title,forename,surname,email,username) VALUES "
                    + " (\""+props.get("role")+"\",\""+props.get("title")+"\",\""+props.get("forename")+"\",\""+props.get("surname")+"\",\""+props.get("email")+"\",\""+props.get("username")+"\");";
            System.out.println(q);
            manager.db.executeUpdate(q);
            q = "SELECT id FROM User WHERE surname=\""+props.get("surname")+"\""+" AND email=\""+props.get("email")+"\" ORDER BY ID DESC LIMIT 1 ";
            System.out.println(q);
            props.put("user",manager.db.executeQuery(q, new String[]{}).get(0).get("id"));
            manager.db.generatePassword((String) props.get("username"), Integer.parseInt((String) props.get("user")));

            q = "SELECT * FROM Degree ";
            props.put("degrees",manager.db.executeQuery(q, new String[]{}));

            q = "SELECT * FROM User ";
            props.put("students",manager.db.executeQuery(q, new String[]{}));

            return new com2008.ui.views.administrator.addStudentAccount(manager,props);

        }else{
            props.put("error","Must be a student");
            return displayStudents(props);
        }
    }

    public ViewInterface addStudentForm(HashMap props) {
        HashMap d = new HashMap();
        d.put("roles",manager.db.executeQuery("Select * FROM Role WHERE name=\"Student\"",new String[]{}));
        ViewInterface view = new com2008.ui.views.administrator.addUserAccount(manager,d);
        return view;
    }


    public ViewInterface displayStudents(HashMap props) {
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT DISTINCT s.id, u.title, u.forename,u.surname,u.username,u.email, d.name,d.yii,d.max_level, s.tutor, ssp.code,ssp.overall_grade,ssp.`level`" +
                " FROM Student s" +
                " INNER JOIN User u ON s.user=u.id" +
                " INNER JOIN StudentStudyPeriod ssp ON s.id = ssp.student" +
                " INNER JOIN StudentModuleDegree smd ON smd.student =  s.id" +
                " INNER JOIN ModuleDegree md ON smd.module = md.id" +
                " INNER JOIN `Degree` d ON md.`degree` = d.id",new String[]{});

        if(props == null) {
            props = new HashMap();
        }
        HashMap<Integer, Object> newProps = new HashMap<>();
        int rowCount = 0;
        for (Map<String,String> row : data ) {
            System.out.println(row);

            newProps.put(Integer.parseInt(row.get("id")),row);
            rowCount+=1;
        }
        props.put("data",newProps);
        System.out.println(props);
        ViewInterface view = new displayStudents(manager, props);

        return view;
    }
}
