package com2008.controllers;
import DbManage.RegistrationManager;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.views.generic.ChangePassword;
import com2008.ui.views.generic.Login;
import com2008.ui.views.generic.Logout;
import com2008.ui.views.registrar.*;
import com2008.ui.ViewInterface;
import com2008.ui.views.registrar.checkCreditSum;
import com2008.ui.views.registrar.displayCreditSum;
import com2008.ui.views.registrar.mainMenu;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GenericController implements Controller {
    public RegistrationManager db;
    public HashMap activities;
    public Manager manager;

    public GenericController(){
        db = null;
    }
    public GenericController(Manager mger) {
        RegistrationManager db = mger.db;
        activities = new HashMap();

        //Define our menu items, and say what functions they correlate to.
        activities.put("Logout","logout");
        activities.put("Login","login");
        //Define Mr. Manager for this controller.
        manager = mger;
    }

    /**
     * Getter for activities.
     * @return activities
     */
    public HashMap getActivities(){
        return activities;
    }

    public ViewInterface login(HashMap props) {
        ViewInterface view = new Login(manager,props);
        return view;
    }

    public ViewInterface logout(HashMap props){
        HashMap d = new HashMap();
        d.put("loggedOut",true);
        ViewInterface view = new Logout(manager,d);
        return view;
    }

    public ViewInterface changePassword(HashMap props){
        HashMap d = new HashMap();
//        d.put("loggedOut",true);
        ViewInterface view = new ChangePassword(manager,d);
        return view;
    }
}