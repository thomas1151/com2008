package com2008.controllers;
import DbManage.RegistrationManager;
import com2008.Manager;
import com2008.ui.views.administrator.*;
import com2008.ui.views.administrator.mainMenu;
import com2008.ui.views.registrar.*;
import com2008.ui.ViewInterface;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdministratorController extends GenericController implements Controller {
    public RegistrationManager db;
    public HashMap activities;
    public Manager manager;

    public AdministratorController(Manager mger) {
        super(mger);

        activities = new HashMap();

        //Define our menu items, and say what functions they correlate to.
        //These functions should require NO INPUT, as it is not programmed
        //to accept.
        //activities.put("Menu","displayMainMenu");
        activities.put("User Accounts","viewUsers");
        activities.put("Degree Course","viewDegreeCourses");
        activities.put("University Departments","viewUniDepartment");
        activities.put("Modules","viewModules");
        activities.put("Logout","logout");
        activities.put("Change Password","changePassword");

        //Define Mr. Manager for this controller.
        manager = mger;

        //Manager has the power to switch the UI, and so this needs to be passed to
        //Views where the ChangeViewListener (or equiv) is being used. If this is not
        //passed, the UI won't be able to update.
    }

    public HashMap getActivities(){
        return activities;
    }

    /**
     * Display the displayUniversityDepartment view passing it all the departments
     * @params props - empty hash map
     * @return view - displayUniversityDepartment view
     */
	public ViewInterface viewUniDepartment(HashMap props) {
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT * "
				+ "FROM Department;", new String[]{});

        HashMap d = new HashMap();
        List<String> id = new ArrayList<>(data.size());
        List<String> name = new ArrayList<>(data.size());
        List<String> code = new ArrayList<>(data.size());

        for (Map<String,String> hash : data){
            id.add(hash.get("id"));
            name.add(hash.get("name"));
            code.add(hash.get("code"));
        }

        d.put("id",id);
        d.put("name",name);
        d.put("code",code);

        ViewInterface view = new displayUniversityDepartments(manager, d);
        return view;
	}

    /**
     * Display the addUniversityDepartment view which gets the details of the department to be added
     * @params props - empty hash map
     * @return view - addUniversityDepartment view
     */
    public ViewInterface displayAddUniDepartment(HashMap props) {
        ViewInterface view = new addUniversityDepartment(manager);
        return view;
    }

    /**
     * Add the department to the database then display the viewUniversityDepartment view
     * @params props - hash map with id, name and code
     * @return view - viewUniversityDepartment view
     */
	public ViewInterface addUniDepartment(HashMap props) {
        ArrayList<Map<String,String>> max  = manager.db.executeQuery("SELECT MAX(id) FROM Department;",new String[]{});
        int num = Integer.parseInt(max.get(0).get("MAX(id)")) + 1;
        manager.db.executeUpdate("INSERT INTO Department "
				+ "VALUES ("+num+",\""+props.get("fullname")+"\",\""+props.get("code")+"\");");
		AdministratorController c = new AdministratorController(this.manager);
        ViewInterface view = c.viewUniDepartment(props);
        return view;
	}

    /**
     * Display the removeUniversityDepartment view which gets the university department to be removed
     * @params props - empty hash map
     * @return view - removeUniversityDepartment view
     */
    public ViewInterface displayRemoveUniDepartment(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("departments", manager.db.executeQuery("SELECT * FROM Department", new String[]{}));
        ViewInterface view = new removeUniversityDepartment(manager,props);
        return view;
    }

    /**
     * Remove the university departments then show the displayUniversityDepartment view
     * @params props - hash map with university department id
     * @return view - displayUniversityDepartment view
     */
	public ViewInterface removeUniDepartment(HashMap props) {
        manager.db.executeUpdate("DELETE "
                + "FROM DepartmentDegree "
                + "WHERE department ="+props.get("id")+";");

        manager.db.executeUpdate("DELETE "
				+ "FROM Department "
				+ "WHERE id ="+props.get("id")+";");

		//Create a new controller so displayUniversityDepartment can run properly
        ViewInterface view = viewUniDepartment(props);
        return view;
	}

    /**
     * Get all the degrees, pass them to getStudentModule and then display the getStudentModule view
     * @params props - empty hash map
     * @return view - getStudentModule view
     */
	public ViewInterface viewDegreeCourses(HashMap props) {
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT * "
				+ "FROM Degree;", new String[]{});

        HashMap d = new HashMap();
        List<String> id = new ArrayList<>(data.size());
        List<String> name = new ArrayList<>(data.size());
        List<String> yii = new ArrayList<>(data.size());
        List<String> max_level = new ArrayList<>(data.size());

        for (Map<String,String> hash : data){
            id.add(hash.get("id"));
            name.add(hash.get("name"));
            yii.add(hash.get("yii"));
            max_level.add(hash.get("max_level"));
        }

        d.put("id",id);
        d.put("name",name);
        d.put("yii",yii);
        d.put("max_level", max_level);


        ViewInterface view = new displayDegreeCourses(manager, d);
        return view;
	}

    /**
     * Display the displayAddDegree view which gets the details of the degree course
     * @params props - empty hash map
     * @return view - addDegreeCourse view
     */
    public ViewInterface displayAddDegreeCourses(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("departments",manager.db.executeQuery("SELECT * FROM Department", new String[]{}));
        ViewInterface view = new addDegreeCourse(manager,props);
        return view;
    }

    /**
     * Add the degree course to the database and display the displayDegreeCourse view
     * @params props - hash map with id, name, yii, max_level, lead, departments
     * @return view - displayDegreeCourses view
     */
	public ViewInterface addDegreeCourse(HashMap props) {
        ArrayList<Map<String,String>> max  = manager.db.executeQuery("SELECT MAX(id) FROM Degree;",new String[]{});
        int num = Integer.parseInt(max.get(0).get("MAX(id)")) + 1;
        manager.db.executeUpdate("INSERT INTO Degree "
				+ "VALUES ("+num+",\""+props.get("name")+"\","+props.get("yii")+",\""+props.get("max_level")+"\");");
        max  = manager.db.executeQuery("SELECT MAX(id) FROM DepartmentDegree;",new String[]{});
        int id;
        if (max.get(0).get("MAX(id)") == null) {
            id = 1;
        }
        else {
            id = Integer.parseInt(max.get(0).get("MAX(id)")) + 1;
        }
        int l;
        if (props.get("lead").equals("yes")) {
            l = 1;
        }
        else {
            l = 0;
        }
        manager.db.executeUpdate("INSERT INTO DepartmentDegree "
				+ "VALUES ("+id+","+l+","+num+","+props.get("department")+");");

        // Create a new controller to run displayDegreeCourse
        AdministratorController c = new AdministratorController(this.manager);
        ViewInterface view = c.viewDegreeCourses(props);
        return view;
	}

    /**
     * Display the removeDegreeCourse view
     * @params props - empty hash map
     * @return view - removeDegreeCourse view
     */
    public ViewInterface displayRemoveDegreeCourse(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("degrees",manager.db.executeQuery("SELECT * FROM Degree", new String[]{}));
        ViewInterface view = new removeDegreeCourse(manager,props);
        return view;
    }

    /**
     * Remove the degree course from the database then display the displayDegreeCourse view
     * @params props - hash map with degree course id
     * @return view - displayDegreeCourse view
     */
	public ViewInterface removeDegreeCourse(HashMap props) {
	    manager.db.executeUpdate("DELETE FROM DepartmentDegree WHERE degree ="+props.get("id")+";");
        manager.db.executeUpdate("DELETE FROM ModuleDegree WHERE degree ="+props.get("id")+";");
        manager.db.executeUpdate("DELETE FROM Degree WHERE id = "+props.get("id")+";");

        //Create a new controller to run displayDegreeCourse properly
        AdministratorController c = new AdministratorController(this.manager);
        ViewInterface view = c.viewDegreeCourses(props);
        return view;
	}

    /**
     * Display the displayModules view which is passed the data in the Module table
     * @params props - empty hash map
     * @return view - displayModules view
     */
	public ViewInterface viewModules(HashMap props) {
	    if(props == null){
	        props = new HashMap();
        }
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT d.name as `degree_name`, m.name as `module_name`, m.ref_code, md.id, md.core,  md.credits, md.taught, md.level " +
                "FROM ModuleDegree md " +
                "LEFT JOIN Degree d ON md.degree = d.id "+
                "LEFT JOIN Module m ON md.module = m.id", new String[]{});
        HashMap d = new HashMap();
        HashMap<Integer, Object> newProps = new HashMap<>();

        int rowCount = 0;
        for (Map<String,String> row : data ) {
            newProps.put(Integer.parseInt(row.get("id")),row);
            rowCount+=1;
        }
        props.put("data",newProps);

        System.out.println(props);
        ViewInterface view = new displayModules(manager, props,"Showing all modules associated with degrees","This page shows all the modules which are currently associated wih degrees at the University. Use the add or remove buttons to change the data.");
        return view;
	}
    public ViewInterface displayUnlinkedModules(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT * FROM Module", new String[]{});
        HashMap d = new HashMap();
        HashMap<Integer, Object> newProps = new HashMap<>();

        for (Map<String,String> row : data ) {
            newProps.put(Integer.parseInt(row.get("id")),row);

        }
        props.put("data",newProps);

        System.out.println(props);
        ViewInterface view = new displayUnlinkedModules(manager, props,"Showing all modules without their associations","This page shows all the modules, regardless of their association with degrees.");
        return view;
    }

    /**
     * Display the addModules view which gets the module details to add
     * @params props - empty hash map
     * @return view - addModules view
     */
    public ViewInterface displayAddModules(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("departments", manager.db.executeQuery("SELECT * FROM Department", new String[]{}));
        ViewInterface view = new addModules(manager,props);
        return view;
    }

    /**
     * Add the module to the database the call addModuleDegree to add the module to ModuleDegree table
     * @params props - hash map with department, fullname, ref, degree, level, module, taught, core, credits
     * @return view - displayModules view
     */
	public ViewInterface addModule(HashMap props) {
        String q = "INSERT INTO Module (name, ref_code) VALUES (  \""+props.get("fullname")+"\",  \""+props.get("ref")+"\")";
	    System.out.println(q);
	    manager.db.executeUpdate(q);

	    if(props == null){
	        props = new HashMap();
        }
        props.put("degrees",manager.db.executeQuery("SELECT * FROM Degree", new String[]{}));
        props.put("levels",manager.db.executeQuery("SELECT * FROM Level", new String[]{}));
        props.put("modules",manager.db.executeQuery("SELECT * FROM Module", new String[]{}));
        props.put("taughts",manager.db.executeQuery("SELECT * FROM ModuleTaught", new String[]{}));
        //Create a new controller to run the displayModules properly
        AdministratorController c = new AdministratorController(this.manager);
        ViewInterface view = c.viewModules(props);
        return view;
	}

    public ViewInterface displayModuleDegree(HashMap props) {
        props.put("degrees",manager.db.executeQuery("SELECT * FROM Degree", new String[]{}));
        props.put("levels",manager.db.executeQuery("SELECT * FROM Level", new String[]{}));
        props.put("modules",manager.db.executeQuery("SELECT * FROM Module", new String[]{}));
        props.put("taughts",manager.db.executeQuery("SELECT * FROM ModuleTaught", new String[]{}));
        ViewInterface view = new addModuleDegree(manager, props);
        return view;
    }

    /**
     * Add the module to the ModuleDegree table then display the displayModules
     * @params props - hash map with degree, level, module, taught, core, credits
     * @return view - displayModules view
     */
	public ViewInterface addModuleDegree(HashMap props) {
	    System.out.println(props);
        String q = "INSERT INTO ModuleDegree (degree,level,module,taught,core,credits) "
                + "VALUES ( \""+props.get("degree")+"\",\""+props.get("level")+"\",\""+props.get("module")+"\",\""+props.get("taught")+"\",\""+props.get("core")+"\",\""+props.get("credits")+"\");";
        System.out.println(q);

        manager.db.executeUpdate(q);
        ViewInterface view = new addModuleDegree(manager, props);
        return view;
	}

    /**
     * Display removeModules which lets the admin input the module to be removed
     * @params props - empty hash map
     * @return view - removeModule view
     */
    public ViewInterface displayRemoveModules(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("modules", manager.db.executeQuery("SELECT * FROM Module", new String[]{}));
        ViewInterface view = new removeModules(manager,props);
        return view;
    }

    /**
     * Remove the module from the database then display the displayModules view
     * @params props - empty hash map
     * @return view - removeModule view
     */
	public ViewInterface removeModule(HashMap props) {
        manager.db.executeUpdate("DELETE "
                + "FROM ModuleDegree "
                + "WHERE module="+props.get("id")+";");

	    String q  = "DELETE "
                + "FROM Module "
                + "WHERE id="+props.get("id")+";";
        manager.db.executeUpdate(q);

		//Create a new controller to run the displayModules properly
        AdministratorController c = new AdministratorController(this.manager);
        ViewInterface view = c.viewModules(props);
        return view;
	}

    /**
     * Display displayUserAccounts view with all the data from the user table
     * @params props - empty hash map
     * @return view - displayUserAccounts view
     */
	public ViewInterface viewUsers(HashMap props) {
        ArrayList<Map<String,String>> data = manager.db.executeQuery("SELECT * "
				+ "FROM User;",new String[] {});

        HashMap d = new HashMap();
        List<String> id = new ArrayList<>(data.size());
        List<String> role = new ArrayList<>(data.size());
        List<String> title = new ArrayList<>(data.size());
        List<String> forename = new ArrayList<>(data.size());
        List<String> surname = new ArrayList<>(data.size());
        List<String> email = new ArrayList<>(data.size());
        List<String> pass = new ArrayList<>(data.size());
        List<String> username = new ArrayList<>(data.size());

        for (Map<String,String> hash : data){
            id.add(hash.get("id"));
            role.add(hash.get("role"));
            title.add(hash.get("title"));
            forename.add(hash.get("forename"));
            surname.add(hash.get("surname"));
            email.add(hash.get("email"));
            pass.add(hash.get("pass"));
            username.add(hash.get("username"));
        }

        d.put("id",id);
        d.put("role",role);
        d.put("title",title);
        d.put("forename", forename);
        d.put("surname",surname);
        d.put("email",email);
        d.put("pass",pass);
        d.put("surname",surname);
        d.put("email",email);
        d.put("pass",pass);
        d.put("username",username);

        ViewInterface view = new displayUserAccounts(manager, d);
        return view;
	}

    /**
     * Display addUserAccount which lets the admin input the user details
     * @params props - empty hash map
     * @return view - removeModule view
     */
    public ViewInterface displayAddUser(HashMap props) {
        HashMap d = new HashMap();
        d.put("roles",manager.db.executeQuery("SELECT * FROM Role",new String[]{}));
        ViewInterface view = new addUserAccount(manager,d);
        return view;
    }

    /**
     * Add the student to the database and then run the displayStudent view
     * @params props - hash map containing the id, degree, title, forename, surname, email, tutor, password
     * @return view - displayStudents view
     */
    public  ViewInterface viewStudents(HashMap props){
        RegistrarController c = new RegistrarController(this.manager);
        ViewInterface view = c.displayStudents(props);
        return view;
    }

    public  ViewInterface addStudentForm(HashMap props){
        RegistrarController c = new RegistrarController(this.manager);
        ViewInterface view = c.addStudentForm(props);
        return view;
    }


    public  ViewInterface addStudent(HashMap props) {

        String q = "INSERT INTO Student (user, degree, tutor) VALUES (\""+props.get("user")+"\",\""+props.get("degree")+"\",\""+props.get("tutor")+"\");";
        System.out.println(q);
        manager.db.executeUpdate(q);

        String studyPeriodId;
        if(props.get("studyperiod") == null){
            studyPeriodId = manager.db.executeQuery("SELECT * " +
                    "FROM StudyPeriod " +
                    "ORDER BY ABS( DATEDIFF( `start`, NOW() ) ) " +
                    "LIMIT 1", new String[]{}).get(0).get("id");
        }else{
            studyPeriodId = (String) props.get("studyperiod");
        }

        String studentID = manager.db.executeQuery("SELECT id FROM Student WHERE tutor=\""+props.get("tutor")+"\"ORDER BY ID DESC LIMIT 1", new String[]{}).get(0).get("id");
        manager.db.executeUpdate("INSERT INTO StudentStudyPeriod (student,studyPeriod,level,code,overall_grade)"
                + "VALUES ("+studentID+","+studyPeriodId+", 1,\"A\",0);");


        ArrayList<Map<String,String>> l = manager.db.executeQuery("SELECT * FROM ModuleDegree WHERE degree = " + props.get("degree") + " AND core = 1", new String[]{});

        for (Map<String,String> hash : l){
            manager.db.executeUpdate("INSERT INTO StudentModuleDegree (module, student,initial_grade,resit_grade)"
                    + "VALUES (" + hash.get("id") + "," + studentID + ",0,0);");
        }
        //Create a new controller to run displayStudents properly
        RegistrarController c = new RegistrarController(this.manager);
        ViewInterface view = c.displayStudents(props);
        return view;
    }

    /**
     * Add the user details into the database and display displayUserAccounts
     * @params props - hash map id, role, user, title, forename, surname, email, password
     * @return view - displayUserAccounts view
     */
	public ViewInterface addUser(HashMap props) {
        String q = "INSERT INTO User (role,title,forename,surname,email,username) VALUES "
                + " (\""+props.get("role")+"\",\""+props.get("title")+"\",\""+props.get("forename")+"\",\""+props.get("surname")+"\",\""+props.get("email")+"\",\""+props.get("username")+"\");";
        System.out.println(q);
        manager.db.executeUpdate(q);

        q = "SELECT id FROM User WHERE surname=\""+props.get("surname")+"\""+" AND email=\""+props.get("email")+"\" ORDER BY ID DESC LIMIT 1 ";
        System.out.println(q);
        props.put("user",manager.db.executeQuery(q, new String[]{}).get(0).get("id"));
        manager.db.generatePassword((String) props.get("username"), Integer.parseInt((String) props.get("user")));


        ViewInterface view = viewUsers(props);
        if(  props.get("role").equals("4")){
            q = "INSERT INTO User (role,title,forename,surname,email,username) VALUES "
					+ " (\""+props.get("role")+"\",\""+props.get("title")+"\",\""+props.get("forename")+"\",\""+props.get("surname")+"\",\""+props.get("email")+"\",\""+props.get("username")+"\");";
			System.out.println(q);
			manager.db.executeUpdate(q);
			q = "SELECT id FROM User WHERE surname=\""+props.get("surname")+"\""+" AND email=\""+props.get("email")+"\" ORDER BY ID DESC LIMIT 1 ";
			System.out.println(q);
            props.put("user",manager.db.executeQuery(q, new String[]{}).get(0).get("id"));
			manager.db.generatePassword((String) props.get("username"), Integer.parseInt((String) props.get("user")));

            q = "SELECT * FROM Degree ";
            props.put("degrees",manager.db.executeQuery(q, new String[]{}));

            q = "SELECT * FROM User ";
            props.put("students",manager.db.executeQuery(q, new String[]{}));
            //This is a student
            System.out.println("This is a student we're adding");
            view = new addStudentAccount(manager, props);
        }
        //Create a new controller to display displayUserAccounts properly
        return view;
	}

    /**
     * Display the removeUserAccount view
     * @params props - empty hash map
     * @return view - removeUserAccount view
     */
    public ViewInterface displayRemoveUser(HashMap props) {
        if(props == null){
            props = new HashMap();
        }
        props.put("users",manager.db.executeQuery("SELECT u.forename, u.surname,s.id FROM Student s LEFT JOIN User u ON s.user=u.id;", new String[]{}));
        ViewInterface view = new removeUserAccount(manager,props);
        return view;
    }

    /**
     * Remove the user account from the database then display the displayUserAccount view
     * @params props - hash map id
     * @return view - displayUserAccount view
     */
	public ViewInterface removeUser(HashMap props) {
        ArrayList<Map<String,String>> n  = manager.db.executeQuery("SELECT role FROM User WHERE id=\""+props.get("id")+"\";",new String[]{});

        if(n.get(0).get("role").equals("4")){
            manager.db.executeUpdate("DELETE "
                    + "FROM Student "
                    + "WHERE user ="+props.get("id")+";");
        }
        manager.db.executeUpdate("DELETE "
				+ "FROM User "
				+ "WHERE id ="+props.get("id")+";");
        ViewInterface view = this.viewUsers(props);
        return view;
	}

    public ViewInterface displayMainMenu(HashMap props) {
	    ViewInterface view = new mainMenu(this.manager);
        return view;
    }
}