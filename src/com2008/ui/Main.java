package com2008.ui;

import com2008.controllers.RegistrarController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Mainly here for old tests, shows UI without a manager.
 */
public class Main {
    public static void main(String[] args) {
        View g = new View("MainFrame",new ArrayList<Map<String,String>>());
        RegistrarController r = new RegistrarController(null);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Create the root.
                Generic sys = new Generic("Registration System", null);
                sys.init();
                sys.pack();
                sys.setVisible(true);

                //Dynamically update content so we know it works fine.
                HashMap props = new HashMap();
                props.put("id","1");
                ViewInterface v = r.sumCredits(props);

                //None of these buttons will work as it is without a manager class.
                sys.setButtons(r.activities);
                sys.setContent(v.getSelf());
                sys.setTitleBar(v.getName());

                //Print all the buttons we have on the UI.
                System.out.println(sys.getAllButtons());

            }
        });
    }
}
