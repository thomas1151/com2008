package com2008.ui.events;

import com2008.Manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * EventListener for changing the view.
 */
public class ChangeViewListener implements ActionListener{

    public Manager manager;
    public String view;
    public HashMap data;

    /**
     * Constructor
     * @param m The Manager that is controlling our UI.
     * @param view The view (as string) we want to change to.
     * @param data Any data that needs sending along the way.
     */
    public ChangeViewListener(Manager m, String view, HashMap data) {
        this.manager = m;
        this.view = view;
        this.data = data;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println(data);
        manager.changeView(view,data);
    }
}
