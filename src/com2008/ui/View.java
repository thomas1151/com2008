package com2008.ui;

import javax.swing.*;
import java.awt.*;

import java.util.ArrayList;
import java.util.Map;


public class View extends JFrame implements ViewInterface {

    JPanel panel = new JPanel();
    public ArrayList<Map<String,String>> data;
    String name = "Untitled View From here";

    public View(){
        super("Untitled View");
    }
    public View(String pname,ArrayList<Map<String,String>> data) {
        super(pname);
        name = pname;
        this.data = data;
        setResizable(true);
    }

    @Override
    public String getName(){
        return name;
    }

    /**
     * Constructor for UI. Should return for JPanel.
     */
    public JPanel getSelf(){
        //This is a sample UI, but essentially any JPanel code can go here.
        FlowLayout paneLayout = new FlowLayout();

        paneLayout.setHgap(5);

        panel.setFont(new Font("Serif", Font.PLAIN, 10));
        JLabel title=  new JLabel("Data here");

        title.setText("Generic View Initialised");

        title.setFont(new Font("Serif", Font.PLAIN, 20));
        panel.add(title);

        return panel;
    }

    /**
     * Initialise the UI, this is used for the createAndShowGUI
     * when debugging the UI/showing independently.
     */
    public void init(){

        this.add(getSelf());
    }


    /**
     * Create the GUI and show it.  For thread safety,
     * this method is invoked from the
     * event dispatch thread.
     */
    public static void createAndShowGUI() {

        JFrame content = new JFrame();
        content.add(new JLabel("Test"));

        //Create and set up the window.
        View frame = new View("Generic Interface",new ArrayList<Map<String,String>>());

        frame.init();
        frame.pack();
        frame.setVisible(true);
    }


    /**
     * This will set up the UI if you try and view it on its own.
     */
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();

            }
        });
    }
}

