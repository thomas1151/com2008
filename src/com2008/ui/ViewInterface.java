package com2008.ui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com2008.ui.View;

/**
 * ViewInterface allows us to create UI views and swap between them.
 */
public interface ViewInterface {
   /**
 * Constructor for UI. Should return for JPanel.
 */
    String name = "Untitled View";
    ArrayList<Map<String,String>> data = new ArrayList<>();

    JPanel getSelf();

    default String getName(){
        return name;
    }

    /**
     * Initialise the UI, this is used for the createAndShowGUI
     * when debugging the UI/showing independently.
     */
    static void init(View v){
        v.add(v.getSelf());
    };

    /**
     * GridBagConstraints, this returns the GridBag to be vertically displayed.
     *
     * @return a GridBagConstraints object with the ....
     */
    static GridBagConstraints getSidebarConstraints() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = GridBagConstraints.RELATIVE;
        return c;
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method is invoked from the
     * event dispatch thread.
     */
    static void createAndShowGUI(View v){
        JFrame content = new JFrame();
        content.add(new JLabel("Test"));

        //Create and set up the window.
        JFrame frame = v;
        ViewInterface.init(v);
        frame.pack();
        frame.setVisible(true);
    }


    static ArrayList< Map<String, String>> HashMapToArrayList(HashMap h){
        ArrayList<Map<String,String>> a = new ArrayList<>();
        a.add(h);
        return a;
    }

    /**
     * This will set up the UI if you try and view it on its own.
     */
    static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI(new View("ViewInterface",new ArrayList<Map<String,String>>()));

            }
        });
    }

}
