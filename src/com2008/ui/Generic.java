
package com2008.ui;
//import com.sun.glass.ui.Size;
import com2008.Manager;
import com2008.ui.components.MenuButton;
import com2008.ui.events.ChangeViewListener;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.ImageIcon;



public class Generic extends JFrame {


    /**
     * Main points of interest in the UI, public so
     * any functions we write can access these.
     */
    JPanel body         = new JPanel();
    JPanel sidebar      = new JPanel();
    JPanel content      = new JPanel();
    JPanel titleBar     = new JPanel();
    JPanel footerBar    = new JPanel();
    JLabel footerText    = new JLabel();
    JLabel title        = new JLabel();
    //ImageIcon titleBarBackground = new ImageIcon("src/com2008/ui/views/pic/titleBar.png");
    Manager manager;

    public Generic(String name, Manager manager) {
        super(name);
        setResizable(true);

        this.manager = manager;
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(WindowEvent winEvt) {
                manager.onExit();
            }
        });
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method is invoked from the
     @param title String to update the title to.
     */
    public void setTitleBar(String title){
        this.title.setText(title);

        //Let us revalidate and repaint
        this.title.revalidate();
        this.title.repaint();
        //this.title.setBackground(Color.red);
    }


    /**
     * Sample FooterBar test.
     *
     */
    public void setFooterBar(String content){
        this.footerText.setText(content);

        //Let us revalidate and repaint
        this.footerText.revalidate();
        this.footerText.repaint();
    }


    /**
     * gets all the buttons in a handy JButton list.
     * @return the list of buttons in the UI.
     */
    public ArrayList<JButton> getAllButtons() {
        ArrayList<JButton> butts = new ArrayList<JButton>();

        Component[] components = this.sidebar.getComponents();
        for (Component component : components) {
            if (component instanceof JButton) {
                butts.add((JButton) component);
            }
        }
        return butts;

    }

    /**
     @param cont The UI components that need to be shown here.
     */
    public void setContent(JPanel cont){
        //We remove all existing as the UI is updating.
        this.content.removeAll();
        this.content.add(cont,BorderLayout.CENTER);
        this.content.updateUI();
        this.revalidate();
        this.content.revalidate();
        this.content.repaint();
        this.content.setBackground(Color.pink);
        this.pack();
        this.repaint();

    }


    /**
     *  Helper function for setButtons which creates new menu button and makes
     *  event listener
     * @param label Label on the button
     * @param view  Destination view as string (local to the controller)
     */
    private void addMenuButton(String label, String view){
        MenuButton b = new MenuButton(label);
        b.addActionListener(new ChangeViewListener(manager,view,null));
        this.sidebar.add(b,getSidebarConstraints());

    }

    /**
     @param actions HashMap of (str,str), construct buttons
     */
    public void setButtons(HashMap actions){
        this.sidebar.removeAll();
        if(actions != null){
            actions.forEach((key, value) -> addMenuButton((String) key,(String) value) );
        }

        this.sidebar.revalidate();
        this.sidebar.repaint();

    }

    /**
     * Defines the layout for Generic
     */
    public void init(){
        //Set pane properties
        BorderLayout paneLayout = new BorderLayout();

        paneLayout.setHgap(5);
        this.setLayout(paneLayout);
        this.setFont(new Font("Broadway BT", Font.PLAIN, 20));

        //Define our page elements

        //In order for the sidebar to show, this needs to be a GridBag
        GridBagLayout sidebarLayout= new GridBagLayout();
        this.sidebar.setLayout(sidebarLayout);
        //this.sidebar.setBackground(Color.red);
        this.sidebar.setFont(new Font("Broadway BT", Font.BOLD, 26));
        // ^set the layout, v set the constraints.
        sidebarLayout.setConstraints(sidebar,getSidebarConstraints());

        this.body.setLayout(new BorderLayout());
        //set the content layout and size.
        //this.content.setBackground(Color.pink);
        this.content.setLayout(new BorderLayout());
        this.content.setFont(new Font("Serif", Font.PLAIN,18));
        this.content.setPreferredSize(new Dimension(1000, 500));

        this.setLocation(400,100);
        //this.setOpacity(1);
        //this.setBackground(Color.pink);
        this.sidebar.setBackground(new Color(23,114,180));
        //this.setBackground(new Color(138,188,209));
        //Set up components preferred size
        MenuButton b = new MenuButton("Just fake button");
        MenuButton br = new MenuButton("Just another button");
        br.setPreferredSize(new Dimension(200,75));
        b.setPreferredSize(new Dimension(200,75));

        //S

        //title.setBackground(Color.pink);
        title.setForeground(new Color(31,32,188));
        title.setFont(new Font("Serif", Font.PLAIN, 40));


        footerText.setText("Not Logged In");
        footerText.setFont(new Font("Serif", Font.PLAIN, 12));

        //Add to the areas of the UI
        //this.titleBar.add(titleBarBackground);
        //****
        this.titleBar.setBackground(Color.white);
        ImageIcon icon = new ImageIcon("src/com2008/ui/views/pic/titlePic.jpg");
        JLabel backgroundLabel = new JLabel(icon);
        backgroundLabel.setBounds(0, 0, icon.getIconWidth(), icon.getIconHeight());
        this.titleBar.add(backgroundLabel);
        this.titleBar.add(title);
        this.footerBar.add(footerText,BorderLayout.WEST);
        this.sidebar.add(br,getSidebarConstraints());
        this.sidebar.add(b,getSidebarConstraints());

        //Add these to the frame
        this.add(titleBar, BorderLayout.PAGE_START);
        this.add(sidebar, BorderLayout.WEST);
        this.add(content, BorderLayout.EAST);
        this.add(footerBar, BorderLayout.PAGE_END);

    }

    /**
     * Constants for the Left menu items
     * @return the GridBagConstraints for the menu
     */
    public GridBagConstraints getSidebarConstraints(){
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = GridBagConstraints.RELATIVE;
        return c;
    }


    /**
     * Create the GUI and show it.  For thread safety,
     * this method is invoked from the
     * event dispatch thread.
     */
    public static void createAndShowGUI() {

        JFrame content = new JFrame();
        content.add(new JLabel("Test"));

        //Create and set up the window.
        Generic frame = new Generic("Generic Interface", null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.init();
        frame.pack();
        frame.setVisible(true);
    }


    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(
                Generic::createAndShowGUI
        );
    }
}