package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addModules extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    HashMap props;
    public addModules(Manager mger, HashMap props) {
        super("Add Modules", new ArrayList<>());
        this.name = "Add Modules";
        manager = mger;
        this.props = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JComboBox department = new JComboBox();
        JTextField fullname = new JTextField();
        JTextField ref = new JTextField();

        JButton btBut4;

        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("departments")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("departments")).get(i);
                String label = user.get("code")+" : "+user.get("name");
                String userID = user.get("id");
                department.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel1 = new JLabel("Enter Department:");
        JLabel lbLabel2 = new JLabel("Enter Fullname:");
        JLabel lbLabel3 = new JLabel("Enter Refrence:");



        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Module"));


        contentPane.add(lbLabel1);
        contentPane.add(department);
        contentPane.add(lbLabel2);
        contentPane.add(fullname);
        contentPane.add(lbLabel3);
        contentPane.add(ref);

        btBut4 = new JButton( "Add Module"  );
        btBut4.addActionListener(new listener(manager, "addModule", new HashMap(), department,fullname,ref));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {
        JComboBox department;
        JTextField fullname;
        JTextField ref;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox department, JTextField fullname, JTextField ref) {
            super(m,view,d);
            this.department = department;
            this.fullname = fullname;
            this.ref = ref;

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = department.getSelectedItem();
            data.put("department", ((ComboItem)item).getValue());
            data.put("fullname",fullname.getText());
            data.put("ref",ref.getText());

            manager.changeView(view,data);
        }
    }
}
