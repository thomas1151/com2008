package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class displayUniversityDepartments extends View implements ViewInterface {
    String name;
    public Manager manager;
    HashMap props;
    public displayUniversityDepartments(Manager mger, HashMap p) {
        super("Display University Departments", new ArrayList<>());
        this.name = "Display University Departments";
        manager = mger;
        props = p;
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JScrollPane tbTable0;
        JButton btBut4;
        JButton btBut5;


        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All User Accounts" ) );

        //pnPanel0.setLayout( new GridLayout(2,0) );

        JPanel buttonsWrap = new JPanel(new GridBagLayout());


        String[] columnNames = new String[props.size()-1];

        String[][] data = new String[ props.size()][((ArrayList<String>) props.get("id")).size()];
        int col = 0;
        for ( Object key : props.keySet()) {
            if(key.equals("id")){
                continue;
            }
            List<String> val = (ArrayList<String>) props.get(key);
            columnNames[col] = (String) key;
            System.out.println(val);
            int valSize = val.size();
            String[] valArray = new String[data[col].length];
            valArray = val.toArray(valArray);

            for (int i = 0; i < valSize; i ++) {
                // i is the index
                data[col][i] = valArray[i];//,
            }
            col +=1;
        }


        tbTable0 = new JScrollPane( new JTable(  transpose(data), columnNames  ));

        btBut4 = new JButton( "Add university department"  );
        btBut4.addActionListener(new add(manager, "displayAddUniDepartment", new HashMap()));
        btBut4.setPreferredSize(new Dimension(200,50));
        buttonsWrap.add( btBut4 );


        btBut5 = new JButton( "Remove university department"  );
        btBut5.addActionListener(new remove(manager, "displayRemoveUniDepartment", new HashMap()));
        btBut5.setPreferredSize(new Dimension(200,50));
        buttonsWrap.add( btBut5 );
        buttonsWrap.setPreferredSize(new Dimension(700,70));
        tbTable0.setPreferredSize(new Dimension(900,400));
        tbTable0.setBackground(new Color(85, 173,201));
        tbTable0.setForeground(Color.darkGray);
        pnPanel0.add(buttonsWrap);
        pnPanel0.add( tbTable0 );

        return pnPanel0;
    }

    private class add extends ChangeViewListener {
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public add(Manager m, String view, HashMap d) {
            super(m,view,d);

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            manager.changeView(view,data);
        }
    }

    private class remove extends ChangeViewListener {
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public remove(Manager m, String view, HashMap d) {
            super(m,view,d);

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            manager.changeView(view,data);
        }
    }
}

