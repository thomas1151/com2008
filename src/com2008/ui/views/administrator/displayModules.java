package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;


public class displayModules extends View implements ViewInterface {
    String name;
    HashMap props;
    public Manager manager;
    String subtitle;
    String description;
    public displayModules(Manager mger, HashMap p, String subtitle, String description) {
        super("Display Modules", new ArrayList<>());
        this.name = "Display Modules";
        this.subtitle = subtitle;
        this.description = description;
        props = p;
        manager = mger;
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JScrollPane tbTable0;
        JButton btBut4;
        JButton btBut5;
        JButton btBut6;
        JButton btBut7;


        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( this.subtitle) );

        //pnPanel0.setLayout( new GridLayout(2,0) );
        JLabel description = new JLabel(this.description);

        JPanel buttonsWrap = new JPanel(new GridBagLayout());

        btBut4 = new JButton( "Add New Module"  );
        btBut4.setPreferredSize(new Dimension(200,50));
        btBut4.addActionListener(new add(manager, "displayAddModules", props));
        buttonsWrap.add( btBut4 );


        Map<Integer,Object> a = ( (Map<Integer, Object>) props.get("data"));

        if(a != null){
            Map<Integer,Map<String,String>> d = (HashMap) props.get("data");
            Map<String,String> firstRow = (Map<String,String>) d.get(d.keySet().toArray()[0]);
            Integer numberOfColumns = firstRow.size();

            //Make an array for our column names
            String[] columnNames = firstRow.keySet().toArray(new String[numberOfColumns]);
            //Set up our table
            String[][] tableData = new String[ ((HashMap) props.get("data")).size()][numberOfColumns];

            int col = 0;
            for ( Object key : d.keySet()) {
                tableData[col] =  ((Map<String,String>) d.get(key)).values().toArray(new String[numberOfColumns]);
                col +=1;
            }

            tbTable0 = new JScrollPane( new JTable(  tableData, columnNames  ));
            tbTable0.setPreferredSize(new Dimension(900, 400));
            //tbTable0.setFont(new Font("Adobe Arabic", Font.PLAIN, 35));
            tbTable0.setForeground(Color.gray);


            btBut5 = new JButton( "Remove Module"  );
            btBut5.setPreferredSize(new Dimension(200,50));
            btBut5.addActionListener(new remove(manager, "displayRemoveModules", props));
            buttonsWrap.add( btBut5 );

            if( !(props.get("registrar") != null && (Boolean) props.get("registrar") == true)){
                btBut6 = new JButton( "Link module and degree"  );
                btBut6.setPreferredSize(new Dimension(200,50));
                btBut6.addActionListener(new remove(manager, "displayModuleDegree", props));
                buttonsWrap.add( btBut6 );

                btBut7 = new JButton( "Show unlinked modules"  );
                btBut7.setPreferredSize(new Dimension(200,50));
                btBut7.addActionListener(new remove(manager, "displayUnlinkedModules", props));
                buttonsWrap.add( btBut7 );
            }
            pnPanel0.add(description);
            pnPanel0.add(buttonsWrap);
            pnPanel0.add( tbTable0 );

        }else{
            pnPanel0.add(buttonsWrap);
            pnPanel0.add(new JLabel("No data exists for this user (yet!). Use the add module button above to add some modules"));
        }
        return pnPanel0;
    }
    private class add extends ChangeViewListener {
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public add(Manager m, String view, HashMap d) {
            super(m,view,d);

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            manager.changeView(view,data);
        }
    }

    private class remove extends ChangeViewListener {
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public remove(Manager m, String view, HashMap d) {
            super(m,view,d);

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            manager.changeView(view,data);
        }
    }
}

