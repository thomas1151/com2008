package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addUniversityDepartment extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public addUniversityDepartment(Manager mger) {
        super("Add University Department", new ArrayList<>());
        this.name = "Add University Department";
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTextField fullname = new JTextField();
        JTextField code = new JTextField();
        //fullname.setPreferredSize(new Dimension(500, 100));
        JButton btBut4;


        JLabel lbLabel2 = new JLabel("Enter Fullname:");
        JLabel lbLabel3 = new JLabel("Enter Code:");


        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));


        contentPane.add(lbLabel2);
        contentPane.add(fullname);
        contentPane.add(lbLabel3);
        contentPane.add(code);

        btBut4 = new JButton( "Add University Department"  );
        btBut4.addActionListener(new listener(manager, "addUniDepartment", new HashMap(),fullname,code));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {
        JTextField fullname;
        JTextField code;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JTextField fullname, JTextField code) {
            super(m,view,d);
            this.fullname = fullname;
            this.code = code;

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            data.put("fullname",fullname.getText());
            data.put("code",code.getText());

            manager.changeView(view,data);
        }
    }
}

