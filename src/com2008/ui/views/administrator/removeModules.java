package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class removeModules extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public HashMap props;
    public removeModules(Manager mger, HashMap props) {
        super("Remove Module", new ArrayList<>());
        this.name = "Remove Module";
        manager = mger;
        this.props = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JButton btBut3;
        JComboBox tfStudentID = new JComboBox();

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Enter Module ID to be removed" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( gbPanel0 );



        Integer moduleLength = ((ArrayList<Map<String,String>>) this.props.get("modules")).size();
        if(moduleLength > 0){
            for(int i =0; i <  moduleLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("modules")).get(i);
                String label = user.get("id")+" : "+user.get("name");
                String userID = user.get("id");
                tfStudentID.addItem(new ComboItem(label,userID));
            }
        }

        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 4;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( tfStudentID, gbcPanel0 );
        pnPanel0.add( tfStudentID );


        btBut3 = new JButton( "Submit"  );
        btBut3.addActionListener(new listener(manager, "removeModule", new HashMap(), tfStudentID));
        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 12;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 5;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( btBut3, gbcPanel0 );
        pnPanel0.add( btBut3 );

        return pnPanel0;
    }
    private class listener extends ChangeViewListener {
        JComboBox text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox text) {
            super(m,view,d);
            this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = text.getSelectedItem();
            data.put("id",((ComboItem)item).getValue() );
            System.out.println("DAta");
            System.out.println(data);
            manager.changeView(view,data);
        }
    }
}
