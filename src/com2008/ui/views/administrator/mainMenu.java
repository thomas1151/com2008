package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;


public class mainMenu extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public mainMenu(Manager mger) {
        super("Main Menu", new ArrayList<>());
        this.name = "Main Menu";
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Administrator Menu" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        pnPanel0.setLayout( gbPanel0 );
        ImageIcon icon = new ImageIcon("src/com2008/ui/views/pic/menuPic.jpg");
        JLabel backgroundLabel = new JLabel(icon);
        backgroundLabel.setBounds(0, 0, icon.getIconWidth(), icon.getIconHeight());
        pnPanel0.add(backgroundLabel);
        return pnPanel0;
    }
}
