package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addModuleDegree extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    HashMap props;
    public addModuleDegree(Manager mger, HashMap p) {
        super("Add Modules Degrees", new ArrayList<>());
        this.name = "Add Modules Degrees";
        manager = mger;
        props = p;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JComboBox degree = new JComboBox();
        JComboBox level = new JComboBox();
        JComboBox module = new JComboBox();
        JComboBox taught = new JComboBox();
        JTextField core = new JTextField();
        JTextField credits = new JTextField();

        JButton btBut4;
        JButton btBut5;


        JLabel lbLabel5 = new JLabel("Enter Degree:");
        System.out.println(props);
        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("degrees")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("degrees")).get(i);
                String label = user.get("id")+" : "+user.get("name");
                String userID = user.get("id");
                degree.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel6 = new JLabel("Enter Level:");
        Integer levelLength = ((ArrayList<Map<String,String>>) this.props.get("levels")).size();
        if(levelLength > 0){
            for(int i =0; i <  levelLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("levels")).get(i);
                String label = user.get("code")+" : "+user.get("name");
                String userID = user.get("code");
                level.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel7 = new JLabel("Enter Module:");

        Integer moduleLength = ((ArrayList<Map<String,String>>) this.props.get("modules")).size();
        if(moduleLength > 0){
            for(int i =0; i <  moduleLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("modules")).get(i);
                String label = user.get("ref_code")+" : "+user.get("name");
                String userID = user.get("id");
                module.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel8 = new JLabel("Enter Taught:");
        Integer taughtLength = ((ArrayList<Map<String,String>>) this.props.get("taughts")).size();
        if(taughtLength > 0){
            for(int i =0; i <  taughtLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("taughts")).get(i);
                String label = user.get("id")+" : "+user.get("name");
                String userID = user.get("id");
                taught.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel9 = new JLabel("Enter Core:");
        JLabel lbLabel10 = new JLabel("Enter Credits:");



        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Module"));


        contentPane.add(lbLabel5);
        contentPane.add(degree);
        contentPane.add(lbLabel6);
        contentPane.add(level);
        contentPane.add(lbLabel7);
        contentPane.add(module);
        contentPane.add(lbLabel8);
        contentPane.add(taught);
        contentPane.add(lbLabel9);
        contentPane.add(core);
        contentPane.add(lbLabel10);
        contentPane.add(credits);

        btBut4 = new JButton( "Add Module Degree"  );
        btBut4.addActionListener(new add(manager, "addModuleDegree", props, degree,level,module,taught,core,credits));

        btBut5 = new JButton( "Finished adding module degrees"  );
        btBut5.addActionListener(new back(manager, "viewModules", new HashMap()));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class add extends ChangeViewListener {
        JComboBox degree;
        JComboBox level;
        JComboBox module;
        JComboBox taught;
        JTextField core;
        JTextField credits;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public add(Manager m, String view, HashMap d, JComboBox degree, JComboBox level, JComboBox module, JComboBox taught, JTextField core, JTextField credits) {
            super(m,view,d);
            this.degree = degree;
            this.level = level;
            this.module = module;
            this.taught = taught;
            this.core = core;
            this.credits = credits;

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = degree.getSelectedItem();
            data.put("degree", ((ComboItem)item).getValue());
            item = level.getSelectedItem();
            data.put("level", ((ComboItem)item).getValue());
            item = module.getSelectedItem();
            data.put("module", ((ComboItem)item).getValue());
            item = taught.getSelectedItem();
            data.put("taught", ((ComboItem)item).getValue());
            data.put("core",core.getText());
            data.put("credits",credits.getText());

            manager.changeView(view,data);
        }
    }

    private class back extends ChangeViewListener{
        JTextField text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public back(Manager m, String view, HashMap d) {
            super(m,view,d);
            //this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            //data.put("module",text.getText());
            manager.changeView(view,data);
        }
    }

}
