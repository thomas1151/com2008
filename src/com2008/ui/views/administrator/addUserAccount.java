package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addUserAccount extends View implements ViewInterface {
    String name;
    HashMap data;
    public Manager manager;
    public addUserAccount(Manager mger, HashMap props) {
        super("Add User Account", new ArrayList<>());
        this.name = "Add User Account";
        manager = mger;
        this.data = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;

//        JTextField role = new JTextField();
        JComboBox role = new JComboBox();
        role.addItem(new ComboItem("Please Select a Role","0"));

        Integer userLength = ((ArrayList<Map<String,String>>) this.data.get("roles")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.data.get("roles")).get(i);
                String label = user.get("name");
                String userID = user.get("id");
                role.addItem(new ComboItem(label,userID));
            }
        }
//        role.setSelectedIndex(3);



        JTextField title = new JTextField();
        JTextField forename = new JTextField();
        JTextField surname = new JTextField();
        JTextField email = new JTextField();
        JTextField username = new JTextField();


        JButton btBut4;

        JLabel lbLabel2 = new JLabel("Enter Role:");
        JLabel lbLabel4 = new JLabel("Enter Title:");
        JLabel lbLabel5 = new JLabel("Enter Forename:");
        JLabel lbLabel6 = new JLabel("Enter Surname:");
        JLabel lbLabel7 = new JLabel("Enter Email:");
        JLabel lbLabel8 = new JLabel("Enter Username:");


        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));




        contentPane.add(lbLabel2);
        contentPane.add(role);


        contentPane.add(lbLabel4);
        contentPane.add(title);
        contentPane.add(lbLabel5);
        contentPane.add(forename);
        contentPane.add(lbLabel6);
        contentPane.add(surname);
        contentPane.add(lbLabel7);
        contentPane.add(email);
        contentPane.add(lbLabel8);
        contentPane.add(username);


        btBut4 = new JButton( "Add User Account"  );
        btBut4.addActionListener(new listener(manager, "addUser", new HashMap(),role,title,forename,surname,email,username));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {


        JComboBox role;

        JTextField title;
        JTextField forename;
        JTextField surname;
        JTextField email;
        JTextField username;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox role, JTextField title, JTextField forename, JTextField surname, JTextField email,JTextField username) {
            super(m,view,d);

            this.role = role;

            this.title = title;
            this.forename = forename;
            this.surname = surname;
            this.email = email;
            this.username = username;


        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = role.getSelectedItem();
            data.put("role",((ComboItem)item).getValue() );
            data.put("username",username.getText());
            data.put("title",title.getText());
            data.put("forename",forename.getText());
            data.put("surname",surname.getText());
            data.put("email",email.getText());


            manager.changeView(view,data);
        }
    }
}

