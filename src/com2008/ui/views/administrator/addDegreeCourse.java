package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addDegreeCourse extends View implements ViewInterface {
    String name;
    HashMap props;
    public Manager manager;
    public addDegreeCourse(Manager mger, HashMap props) {
        super("Add Degree Course", new ArrayList<>());
        this.name = "Add Degree Course";
        manager = mger;
        this.props = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTextField name = new JTextField();
        JTextField yii = new JTextField();
        JTextField max_level = new JTextField();
        JTextField lead = new JTextField();
        JComboBox department = new JComboBox();

        JButton btBut4;

        Integer moduleLength = ((ArrayList<Map<String,String>>) this.props.get("departments")).size();
        if(moduleLength > 0){
            for(int i =0; i <  moduleLength; i++){
                Map<String,String> row = ((ArrayList<Map<String,String>>) this.props.get("departments")).get(i);
                String label = row.get("code")+" : "+row.get("name");
                String userID = row.get("id");
                department.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel2 = new JLabel("Enter name:");
        JLabel lbLabel3 = new JLabel("Enter yii:");
        JLabel lbLabel4 = new JLabel("Enter max_level:");
        JLabel lbLabel5 = new JLabel("Enter lead (yes/no):");
        JLabel lbLabel6 = new JLabel("Enter department id:");


        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));


        contentPane.add(lbLabel2);
        contentPane.add(name);
        contentPane.add(lbLabel3);
        contentPane.add(yii);
        contentPane.add(lbLabel4);
        contentPane.add(max_level);
        contentPane.add(lbLabel5);
        contentPane.add(lead);
        contentPane.add(lbLabel6);
        contentPane.add(department);


        btBut4 = new JButton( "Add Degree Course"  );
        btBut4.addActionListener(new listener(manager, "addDegreeCourse", new HashMap(),name,yii,max_level,lead,department));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {
        JTextField name;
        JTextField yii;
        JTextField max_level;
        JTextField lead;
        JComboBox department;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JTextField name, JTextField yii, JTextField max_level, JTextField lead, JComboBox department) {
            super(m,view,d);
            this.name = name;
            this.yii = yii;
            this.max_level = max_level;
            this.lead = lead;
            this.department = department;

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            data.put("name",name.getText());
            data.put("yii",yii.getText());
            data.put("max_level",max_level.getText());
            data.put("lead",lead.getText());

            Object item = department.getSelectedItem();
            data.put("department",((ComboItem)item).getValue() );

            manager.changeView(view,data);
        }
    }
}

