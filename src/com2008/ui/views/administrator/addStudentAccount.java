package com2008.ui.views.administrator;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addStudentAccount extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public HashMap props;
    public addStudentAccount(Manager mger,HashMap props) {
        super("Add Student", new ArrayList<>());
        this.name = "Add Student";
        manager = mger;
        this.props = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;

//        JTextField degree = new JTextField();
        JTextField user   = new JTextField();
        JTextField tutor = new JTextField();

        JComboBox students = new JComboBox();
        students.addItem(new ComboItem("Please Select a Student","0"));

//        System.out.println("PROPS OF ADDSTUDENT ==================");
        System.out.println(props);
        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("students")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> row = ((ArrayList<Map<String,String>>) this.props.get("students")).get(i);
                String label = row.get("title")+" "+row.get("forename")+" "+row.get("surname");
                String userID = row.get("id");

                System.out.println(row);
                students.addItem(new ComboItem(label,userID));
                if(userID.equals(this.props.get("user"))){
                    students.setSelectedIndex(i+1);
                }
            }
        }

        JComboBox degree = new JComboBox();
        degree.addItem(new ComboItem("Please Select a Degree","0"));
        Integer degreeLength = ((ArrayList<Map<String,String>>) this.props.get("degrees")).size();
        if(userLength > 0){
            for(int i =0; i <  degreeLength; i++){
                Map<String,String> row = ((ArrayList<Map<String,String>>) this.props.get("degrees")).get(i);
                String label = row.get("name");
                String userID = row.get("id");
                degree.addItem(new ComboItem(label,userID));
            }
        }





        JButton btBut4;

        JLabel lbLabel2 = new JLabel("Select Degree CODE:");
        JLabel lbLabel3 = new JLabel("Select User:");
        JLabel lbLabel4 = new JLabel("Enter Tutor Name:");


        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));



        contentPane.add(lbLabel3);
        contentPane.add(students);


        contentPane.add(lbLabel2);
        contentPane.add(degree);


        contentPane.add(lbLabel4);
        contentPane.add(tutor);


        btBut4 = new JButton( "Add Student Account"  );
        btBut4.addActionListener(new listener(manager, "addStudent", new HashMap(),students,degree,tutor));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {


        JComboBox degree;
        JTextField tutor;
        JComboBox user;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox user, JComboBox degree, JTextField tutor) {
            super(m,view,d);
            this.user = user;
            this.degree = degree;
            this.tutor = tutor;


        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = user.getSelectedItem();
            data.put("user",((ComboItem)item).getValue() );

            item = degree.getSelectedItem();
            data.put("degree",((ComboItem)item).getValue() );

            data.put("tutor",tutor.getText());

            manager.changeView(view,data);
        }
    }
}

