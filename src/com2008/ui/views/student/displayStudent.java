package com2008.ui.views.student;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;


public class displayStudent extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data = new ArrayList<>();
    HashMap props;
    public Manager manager;
    public Map<String,String> userDetails;
    public HashMap levelDetails;
    public Map<Integer, Object> moduleDetails;
    public displayStudent(Manager mger, HashMap p) {
        super("Display student", new ArrayList<>());
        this.name = "Display Students";
        manager = mger;
        props = p;
        userDetails = ((Map<String,String>) props.get("userDetails"));
        levelDetails = (HashMap) props.get("levels");
        moduleDetails = (Map<Integer, Object>) props.get("moduleDetails");
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }


    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JScrollPane tbTable0;


        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( new BoxLayout(pnPanel0,1) );



        System.out.println(props);


        JLabel name = new JLabel("Name: "+userDetails.get("title")+" "+userDetails.get("forename")+" "+userDetails.get("surname"));
        JLabel degree = new JLabel("Degree: "+userDetails.get("degree_name"));
        JLabel tutor = new JLabel("Tutor: "+userDetails.get("tutor"));
        JLabel username = new JLabel("Username: "+userDetails.get("username"));
        JLabel email = new JLabel("Email: "+userDetails.get("email"));
        JLabel studentID = new JLabel("Student ID: "+userDetails.get("student_id"));
        JLabel currentLevel = new JLabel("Current Level: "+userDetails.get("level_name"));

        pnPanel0.add( name );
        pnPanel0.add( degree );
        pnPanel0.add( tutor );
        pnPanel0.add( username );
        pnPanel0.add( email );
        pnPanel0.add( studentID );
        pnPanel0.add( currentLevel );


        HashMap<Integer, JLabel> levelLabels = new HashMap<>();
        for (Object key : levelDetails.keySet()) {
            String ourKey = (String) key;
            Integer levelOfInterest = 0;
            if(ourKey.contains("_possible")){
                levelOfInterest = Integer.parseInt(ourKey.replace("_possible",""));
                if(levelLabels.get(levelOfInterest) == null){
                    levelLabels.put(levelOfInterest, new JLabel());
                }
                levelLabels.get(levelOfInterest).setText(levelLabels.get(levelOfInterest).getText() + "out of a possible "+levelDetails.get(key));
                pnPanel0.add(levelLabels.get(levelOfInterest));
            }else{
                levelOfInterest = Integer.parseInt(ourKey);
                if(levelLabels.get(levelOfInterest) == null){
                    levelLabels.put(levelOfInterest, new JLabel());
                }
                levelLabels.get(levelOfInterest).setText("Level "+levelOfInterest+": "+levelDetails.get(key)+ " credits "+ levelLabels.get(levelOfInterest).getText());
                pnPanel0.add(levelLabels.get(levelOfInterest));
            }


        }


        if(moduleDetails != null){
            Map<Integer,Map<String,String>> d = (HashMap) moduleDetails;
            Map<String,String> firstRow = (Map<String,String>) d.get(d.keySet().toArray()[0]);
            Integer numberOfColumns = firstRow.size();

            //Make an array for our column names
            String[] columnNames = firstRow.keySet().toArray(new String[numberOfColumns]);
            //Set up our table
            String[][] tableData = new String[moduleDetails.size()][numberOfColumns];

            int col = 0;
            for ( Object key : d.keySet()) {
                tableData[col] =  ((Map<String,String>) d.get(key)).values().toArray(new String[numberOfColumns]);
                col +=1;
            }

            tbTable0 = new JScrollPane(new JTable(tableData, columnNames));
            gbcPanel0.gridx = 2;
            gbcPanel0.gridy = 2;
            gbcPanel0.gridwidth = 16;
            gbcPanel0.gridheight = 16;
            gbcPanel0.fill = GridBagConstraints.BOTH;
            gbcPanel0.weightx = 1;
            gbcPanel0.weighty = 1;
            gbcPanel0.anchor = GridBagConstraints.NORTH;
            gbPanel0.setConstraints( tbTable0, gbcPanel0 );
            pnPanel0.add( tbTable0 );
        }
//        System.out.println(Arrays.deepToString(data));
//        System.out.println(data);
//
//        tbTable0 = new JScrollPane(new JTable( transpose(data), columnNames ));



        return pnPanel0;
    }

}
