package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayDegreeResult extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    HashMap props;
    public Manager manager;
    double classification;
    Map<Integer,Double> levels;
    public displayDegreeResult(Manager mger, HashMap p) {
        super("Display Degree Results", new ArrayList<>());
        this.name = "Display Degree Results";
        manager = mger;
        props = p;
        levels =  (Map<Integer,Double>)props.get("levels");
        classification = 0;
        int numberOfLevels = levels.size();
        if(numberOfLevels > 0){
            double initialWeight = 1/numberOfLevels;
            double totalWeights = 0;
            System.out.println(initialWeight);
            for (Map.Entry<Integer, Double> entry : levels.entrySet()) {
                Integer key = entry.getKey();
                Double value = entry.getValue();
                double weight = initialWeight;

                switch(key){
                    case(2): weight *= 0.5; break;
                    default: break;
                }
                totalWeights = totalWeights + weight;
                
                classification += (weight*value);

            }
            classification = classification / totalWeights;
        }

    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JLabel label;
        JLabel grade;
        JTable tbTable0;
        JButton btBut4;


        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );
        pnPanel0.setLayout( new BoxLayout(pnPanel0,1) );


        label = new JLabel("Score of " +classification);
        pnPanel0.add( label );

        if (classification >= 70) {
            grade = new JLabel("They got a First");
            pnPanel0.add( grade );
        }
        else if (classification >= 60) {
            grade = new JLabel("They got a 2:1");
            pnPanel0.add( grade );
        }
        else if (classification >= 50) {
            grade = new JLabel("They got a 2:2");
            pnPanel0.add( grade );
        }
        else if (classification >= 40) {
            grade = new JLabel("They got a Pass");
            pnPanel0.add( grade );
        }
        else {
            grade = new JLabel("They Failed");
            pnPanel0.add( grade );
        }

        return pnPanel0;
    }

}
