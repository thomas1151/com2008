package com2008.ui.views.teacher;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com2008.ui.View;
import com2008.ui.ViewInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;


public class student_perform extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public student_perform(ArrayList<Map<String,String>> data) {
        super("Student Perform", data);
        this.name = "Student Perform";
    }

    @Override
    public JPanel getSelf() {

        JLayeredPane layeredPane = new JLayeredPane();
        JPanel panel = new JPanel();

        layeredPane.add(panel, new Integer(2));
//        panel.setLayout(null);
        // student Name
        JButton userNamebtn = new JButton("User Name");
        userNamebtn.setBounds(200, 40, 150, 50);
        userNamebtn.setFont(new Font("Broadway BT",Font.BOLD,15));
        userNamebtn.setBorder(BorderFactory.createEtchedBorder(Color.cyan, Color.red));
        userNamebtn.setForeground(Color.white);
        userNamebtn.setBackground(Color.PINK);
        panel.add(userNamebtn);

        ImageIcon subTitleBackground = new ImageIcon("picture\\subtitlebackground.png");
        JLabel subTitle = new JLabel(subTitleBackground);
        subTitle.setBounds(370, 40, 780, 50);
        subTitle.setForeground(Color.pink);
        subTitle.setFont(new Font("Broadway BT",Font.BOLD,36));
        panel.add(subTitle);
        // student ID
        JLabel studentID = new JLabel("ID: ");
        studentID.setBounds(130, 140, 200, 50);
        studentID.setOpaque(true);
        studentID.setBackground(Color.gray);
        studentID.setBorder(BorderFactory.createLineBorder(Color.white));
        studentID.setForeground(Color.pink);
        studentID.setFont(new Font("Broadway BT",Font.BOLD,20));
        panel.add(studentID);
        //level
        JLabel studentLevel = new JLabel("Level: ");
        studentLevel.setBounds(350, 140, 200, 50);
        studentLevel.setOpaque(true);
        studentLevel.setBackground(Color.gray);
        studentLevel.setBorder(BorderFactory.createLineBorder(Color.white));
        studentLevel.setForeground(Color.pink);
        studentLevel.setFont(new Font("Broadway BT",Font.BOLD,20));
        panel.add(studentLevel);
        // department
        JLabel studentDepartment = new JLabel("Department: ");
        studentDepartment.setBounds(570, 140, 400, 50);
        studentDepartment.setOpaque(true);
        studentDepartment.setBackground(Color.gray);
        studentDepartment.setBorder(BorderFactory.createLineBorder(Color.white));
        studentDepartment.setForeground(Color.pink);
        studentDepartment.setFont(new Font("Broadway BT",Font.BOLD,16));
        panel.add(studentDepartment);
        // degree
        JLabel studentDegree = new JLabel("Degree: ");
        studentDegree.setBounds(990, 140, 200, 50);
        studentDegree.setOpaque(true);
        studentDegree.setBackground(Color.gray);
        studentDegree.setBorder(BorderFactory.createLineBorder(Color.white));
        studentDegree.setForeground(Color.pink);
        studentDegree.setFont(new Font("Broadway BT",Font.BOLD,20));
        panel.add(studentDegree);

        // modules
        Object[][] detials = {
                // data from database
                // example
                {"MODULES","MODULES CODE","GRADE"}, // table head
                {"CS","COM2008","100"},	// table body
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"},
                {"CS","COM2008","100"}
        };

        String[] modules = {"MODULES","MODULES CODE", "GRADE"};
        JTable table = new JTable(detials,modules);
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFont(new Font("Broadway BT",Font.BOLD,12));
        table.setForeground(Color.DARK_GRAY);
        table.setBackground(Color.pink);
        table.setBorder(BorderFactory.createLineBorder(null, 2));
        table.setBounds(120, 210, 700, 470);
        panel.add(table);

        // achievement
        Object[][] achievements = {
                {"OBJECTS","ACHIEVEMENT"},
                {"match","award"},
                {"match","award"},
                {"match","award"}
        };
        String[] title = {"OBJECTS","ACHIEVEMENT"};
        JTable table2 = new JTable(achievements,title);
        table2.setFont(new Font("Broadway BT",Font.LAYOUT_LEFT_TO_RIGHT,12));
        table2.setForeground(Color.DARK_GRAY);
        table2.setBackground(Color.pink);
        table2.setBounds(830, 210, 405, 470);
        table2.setBorder(BorderFactory.createLineBorder(null, 2));
        panel.add(table2);

        // others
        ImageIcon detialBackground = new ImageIcon("picture\\detial_background.png");
        JLabel detialPic = new JLabel(detialBackground);
        detialPic.setBounds(90, 120, detialBackground.getIconWidth(), detialBackground.getIconHeight());
        panel.add(detialPic);

        //whole background pic
        ImageIcon icon = new ImageIcon("picture\\CMS-NEBNight.png");
        JLabel backgroundLabel = new JLabel(icon);
        backgroundLabel.setBounds(0, 0, icon.getIconWidth(), icon.getIconHeight());
        panel.add(backgroundLabel);

        return panel;
    }
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ViewInterface.createAndShowGUI(new student_perform(null));
            }
        });
    }
}




//public class student_perform {
//    static JFrame jFrame = new JFrame();
//    public static void main(String[] args) {
//        // TODO Auto-generated method stub
//        student_perform a = new student_perform();
//        a.initFrame();
//    }
//
//    public student_perform() {
//        initFrame();
//    }
//
//    public void initFrame() {
//
//    }
//
//    public void placeComponents(JPanel panel) {
//
//
//    }
//
//}
