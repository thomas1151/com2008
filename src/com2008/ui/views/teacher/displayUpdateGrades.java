package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;
import com2008.ui.events.registar.AddModuleListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayUpdateGrades extends View implements ViewInterface {
    String name;
    HashMap props;
    public Manager manager;
    public displayUpdateGrades(Manager mger, HashMap props) {
        super("Add module", new ArrayList<>());
        this.name = "Add module";
        this.props = props;
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JTextField grade = new JTextField();
        JComboBox module = new JComboBox();

        JButton btBut4;

        JLabel lbLabel8 = new JLabel("Enter the module:");
        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("modules")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("modules")).get(i);
                String label = user.get("ref_code")+" : "+user.get("name");
                String userID = user.get("id");
                module.addItem(new ComboItem(label,userID));
            }
        }

        JLabel lbLabel10 = new JLabel("Enter score:");



        JPanel contentPane = new JPanel();
        contentPane.setLayout( new BoxLayout(contentPane,BoxLayout.Y_AXIS) );
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));

        contentPane.add(lbLabel8);
        contentPane.add(module);
        contentPane.add(lbLabel10);
        contentPane.add(grade);

        btBut4 = new JButton( "Add student"  );

        btBut4.addActionListener(new listener(manager, "updateGrades", props, module, grade));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {
        JComboBox module;
        JTextField grade;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox module, JTextField grade) {
            super(m,view,d);
            this.module = module;
            this.grade = grade;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = module.getSelectedItem();
            data.put("module",((ComboItem)item).getValue() );
            data.put("grade",grade.getText());
            System.out.println(data);
            manager.changeView(view,data);
        }
    }

}

