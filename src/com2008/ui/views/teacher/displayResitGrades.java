package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayResitGrades extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    HashMap props;
    public Manager manager;
    public displayResitGrades(Manager mger, HashMap p) {
        super("Update Resit Grades", new ArrayList<>());
        this.name = "Update Resit Grades";
        manager = mger;
        props = p;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTextField grade = new JTextField();

        grade.setPreferredSize(new Dimension(200,40));

        JButton btBut4;
        JLabel lbLabel10 = new JLabel("Enter score:");

        JComboBox module = new JComboBox();
        module.setPreferredSize(new Dimension(200,40));

        JLabel lbLabel8 = new JLabel("Enter the module:");
        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("modules")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("modules")).get(i);
                String label = user.get("ref_code")+" : "+user.get("name");
                String userID = user.get("id");
                module.addItem(new ComboItem(label,userID));
            }
        }


        pnPanel0 = new JPanel();

        pnPanel0.add(lbLabel8);
        pnPanel0.add(module);
        pnPanel0.add(lbLabel10);
        pnPanel0.add(grade);

        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Update resit Grades" ) );
        //GridBagLayout gbPanel0 = new GridBagLayout();
        //GridBagConstraints gbcPanel0 = new GridBagConstraints();
        //pnPanel0.setLayout( gbPanel0 );

        btBut4 = new JButton( "Add resit grades"  );
        btBut4.setBackground(new Color(85,152,195));
        btBut4.setPreferredSize(new Dimension(200,40));
        btBut4.addActionListener(new listener(manager, "updateResitGrades", props, module, grade));

        /*gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 16;
        gbcPanel0.gridwidth = 7;
        gbcPanel0.gridheight = 2;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( btBut4, gbcPanel0 );*/
        pnPanel0.add( btBut4 );

        return pnPanel0;
    }

    private class listener extends ChangeViewListener {
        JComboBox module;
        JTextField grade;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox module, JTextField grade) {
            super(m,view,d);
            this.module = module;
            this.grade = grade;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = module.getSelectedItem();
            data.put("module",((ComboItem)item).getValue() );
            data.put("grade",grade.getText());

            manager.changeView(view,data);
        }
    }
}
