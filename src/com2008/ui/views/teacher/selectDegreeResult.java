package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class selectDegreeResult extends View implements ViewInterface {
    String name;
    public Manager manager;
    public HashMap props;
    public selectDegreeResult(Manager mger,HashMap props) {
        super("Check Students Degree Result", new ArrayList<>());
        this.name = "Check Students Degree Result";
        manager = mger;
        this.props = props;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JButton btBut4;
        JComboBox id = new JComboBox<>();
        id.setPreferredSize(new Dimension(400,50));

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );

        //pnPanel0.setLayout( new BoxLayout(pnPanel0,BoxLayout.Y_AXIS) );
        JLabel lbLabel = new JLabel("Enter the module:");
        Integer userLength = ((ArrayList<Map<String,String>>) this.props.get("users")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.props.get("users")).get(i);
                String label = user.get("id")+" : "+user.get("forename")+" "+user.get("surname");
                String userID = user.get("id");
                id.addItem(new ComboItem(label,userID));
            }
        }

        JLabel label = new JLabel("Select the user to check");

        pnPanel0.add(label);
        pnPanel0.add(id);

        btBut4 = new JButton( "Select the student to check the result"  );
        btBut4.setPreferredSize(new Dimension(200,50));
        btBut4.setBackground(new Color(85,152,195));
        btBut4.addActionListener(new listener(manager, "calculateGrade", props, id));

        pnPanel0.add( btBut4 );


        //buttons

        return pnPanel0;
    }

    private class listener extends ChangeViewListener {
        JComboBox id;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox id) {
            super(m,view,d);
            this.id = id;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = id.getSelectedItem();
            data.put("id",((ComboItem)item).getValue() );
            manager.changeView(view,data);
        }
    }

}
