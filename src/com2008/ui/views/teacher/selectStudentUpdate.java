package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class selectStudentUpdate extends View implements ViewInterface {
    String name;
    HashMap data;
    public Manager manager;
    public Map<Integer, Object> studentGrades;

    public selectStudentUpdate(Manager mger, HashMap p) {
        super("Select Student to Update", new ArrayList<>());
        this.name = "Display Student to Update";
        manager = mger;
        data = p;
        studentGrades = (Map<Integer, Object>) p.get("studentGrades");

    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JButton btBut4;
        JButton btBut5;
        JScrollPane tbTable0;
        JComboBox tfStudentID = new JComboBox( );
        tfStudentID.addItem(new ComboItem("Please Select a Student","0"));


        Integer userLength = ((ArrayList<Map<String,String>>) this.data.get("users")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.data.get("users")).get(i);
                String label = user.get("id")+" : "+user.get("forename") +" "+user.get("surname");
                String userID = user.get("id");
                tfStudentID.addItem(new ComboItem(label,userID));
            }
        }


        tfStudentID.setPreferredSize(new Dimension(400,50));

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );
        //pnPanel0.setLayout( new BoxLayout(pnPanel0,BoxLayout.Y_AXIS) );



        JLabel label = new JLabel("Select the user to check");

        pnPanel0.add(label);
        pnPanel0.add(tfStudentID);

        btBut4 = new JButton( "Update the initial grade"  );
        btBut4.setPreferredSize(new Dimension(200,50));
        btBut4.setBackground(new Color(85,152,195));
        btBut4.addActionListener(new initial(manager, "displayUpdateGrades", new HashMap(), tfStudentID));

        btBut5 = new JButton( "Update the resit grade"  );
        btBut5.setPreferredSize(new Dimension(200,50));
        btBut5.setBackground(new Color(85,152,195));
        btBut5.addActionListener(new resit(manager, "displayUpdateResit", new HashMap(), tfStudentID));

        if(studentGrades != null) {
            Map<Integer, Map<String, String>> d = (HashMap) studentGrades;
            Map<String, String> firstRow = (Map<String, String>) d.get(d.keySet().toArray()[0]);
            Integer numberOfColumns = firstRow.size();

            //Make an array for our column names
            String[] columnNames = firstRow.keySet().toArray(new String[numberOfColumns]);
            //Set up our table
            String[][] tableData = new String[studentGrades.size()][numberOfColumns];

            int col = 0;
            for (Object key : d.keySet()) {
                tableData[col] = ((Map<String, String>) d.get(key)).values().toArray(new String[numberOfColumns]);
                col += 1;
            }

            tbTable0 = new JScrollPane(new JTable(tableData, columnNames));
            pnPanel0.add( tbTable0 );

        }
        
        pnPanel0.add( btBut4 );
        pnPanel0.add(btBut5);


        //buttons

        return pnPanel0;
    }

    private class initial extends ChangeViewListener {
        JComboBox id;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public initial(Manager m, String view, HashMap d, JComboBox id) {
            super(m,view,d);
            this.id = id;

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = id.getSelectedItem();
            data.put("id",((ComboItem)item).getValue() );
            manager.changeView(view,data);
        }
    }

    private class resit extends ChangeViewListener {
        JComboBox id;

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public resit(Manager m, String view, HashMap d, JComboBox id) {
            super(m,view,d);
            this.id = id;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = id.getSelectedItem();
            data.put("id",((ComboItem)item).getValue() );
            manager.changeView(view,data);
        }
    }

}
