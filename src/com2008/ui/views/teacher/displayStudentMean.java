package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayStudentMean extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    HashMap props;
    public Manager manager;
    public displayStudentMean(Manager mger, HashMap p) {
        super("Display student", new ArrayList<>());
        this.name = "Display Students";
        manager = mger;
        props = p;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JLabel result;
        JButton btBut1;
        JButton btBut2;
        JButton btBut3;
        JButton btBut4;
        JLabel grade;



        double mean = (double) props.get("mean");
        HashMap userDetails = (HashMap) props.get("student");
        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );

        pnPanel0.setLayout( new BoxLayout(pnPanel0,BoxLayout.Y_AXIS) );

        JLabel name = new JLabel("Name: "+userDetails.get("title")+" "+userDetails.get("forename")+" "+userDetails.get("surname"));
        JLabel degree = new JLabel("Degree: "+userDetails.get("degree_name"));
        JLabel tutor = new JLabel("Tutor: "+userDetails.get("tutor"));
        JLabel username = new JLabel("Username: "+userDetails.get("username"));
        JLabel email = new JLabel("Email: "+userDetails.get("email"));
        JLabel studentID = new JLabel("Student ID: "+userDetails.get("student_id"));
        JLabel currentLevel = new JLabel("Current Level: "+userDetails.get("level_name"));
        JLabel existingGrade = new JLabel("Current Grade %: "+mean);

        pnPanel0.add( name );
        pnPanel0.add( degree );
        pnPanel0.add( tutor );
        pnPanel0.add( username );
        pnPanel0.add( email );
        pnPanel0.add( studentID );
        pnPanel0.add( currentLevel );
        pnPanel0.add( existingGrade );


        System.out.println("PROPS");
        System.out.println(props);

        if (mean >= 70) {
            grade = new JLabel("They got a First");
            pnPanel0.add( grade );
        }
        else if (mean>= 60) {
            grade = new JLabel("They got a Two One");
            pnPanel0.add( grade );
        }
        else if (mean  >= 50) {
            grade = new JLabel("They got a Two Two");
            pnPanel0.add( grade );
        }
        else if (mean  >= 40) {
            grade = new JLabel("They got a Pass");
            pnPanel0.add( grade );
        }
        else {
            grade = new JLabel("They Failed");
            pnPanel0.add( grade );
        }

        btBut1 = new JButton( "Fail the student"  );
        btBut1.addActionListener(new ChangeViewListener(manager, "updateResitGrades", props));

        btBut2 = new JButton( "Graduate the student"  );
        btBut2.addActionListener(new ChangeViewListener(manager, "graduateStudent", props));

        btBut3 = new JButton( "Progress the student"  );
        btBut3.addActionListener(new ChangeViewListener(manager, "progressStudent", props));

        btBut4 = new JButton( "Set student to retake the year"  );
        btBut4.addActionListener(new ChangeViewListener(manager, "failStudent", props));

        pnPanel0.add( btBut1 );
        pnPanel0.add( btBut2 );
        pnPanel0.add( btBut3 );
        pnPanel0.add( btBut4 );

        //buttons

        return pnPanel0;
    }

}
