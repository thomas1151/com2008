package com2008.ui.views.teacher;

import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class mainMenu extends View implements ViewInterface{
    String name;
    public Manager manager;
    public mainMenu(Manager mger) {
        super("Main Menu", new ArrayList<>());
        manager = mger;

    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "student information" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( gbPanel0 );

//        String modules = {
//                { this.modules.get(0).get("moduleID"),this.modules.get(0).get("moduleTitle"),this.modules.get(0).get("departments") },
//                { "COM1008", "programming", "Compter Science" }
//        };
        String[] columName0 = {"modules ID","modules title","departments"};
//        String[][] rows = new String[y][3];
//        int y = this.modules.size();
//        for (int x = 0; x < y; x++) {
//            rows[x] = new String[] {
//                    this.modules.get(x).get("moduleID"),
//                    this.modules.get(x).get("moduleTitle"),
//                    this.modules.get(x).get("id")
//            };
//        }
//        tbTable0 = new JTable( rows, columName0 );
//
//        String years = {
//                { this.modules.get(0).get("year"),this.modules.get(0).get("outcome")},
//                { "first", "20" }
//        };
//        String[] columName1 = {"year","outcome"}
//        String[][] rows1 = new String[y][1];
//        int y = this.years.size();
//        for (int x = 0; x < y; x++) {
//            rows1[x] = new String[] {
//                    this.years.get(x).get("year"),
//                    this.years.get(x).get("outcome"),
//            };
//        }
//        tbTable1 = new JTable( rows1, columName1 );

        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 2;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 7;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 1;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
//        gbPanel0.setConstraints( tbTable0, gbcPanel0 );
//        pnPanel0.add( tbTable0 );

        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 2;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 7;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 1;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
//        gbPanel0.setConstraints( tbTable1, gbcPanel0 );
//        pnPanel0.add( tbTable1 );

        return pnPanel0;

    }
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ViewInterface.createAndShowGUI(new mainMenu(null));
            }
        });
    }

}