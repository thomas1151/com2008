package com2008.ui.views.teacher;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayStudentDetails extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    HashMap props;
    public Manager manager;
    public displayStudentDetails(Manager mger, HashMap p) {
        super("Display Student Modules", new ArrayList<>());
        this.name = "Display Students Modules";
        manager = mger;
        props = p;
}

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTable tbTable0;
        JButton btBut4;
        JButton btBut5;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder(BorderFactory.createTitledBorder("All Student Modules"));
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout(gbPanel0);

        String[] coloumnNames = {"id", "module","initial_grade","resit_grade"};
        String[] id = (String[]) props.get("id");
        String[] module = (String[]) props.get("module");
        String[] initial_grade = (String[]) props.get("initial_grade");
        String[] resit_grade = (String[]) props.get("resit_grade");

        int y = id.length;
        String[][] rows = new String[y][7];
        for (int x = 0; x < y; x++) {
            rows[x] = new String[]{id[x],
                    module[x],
                    initial_grade[x],
                    resit_grade[x]};
        }
        tbTable0 = new JTable(rows, coloumnNames);
        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 2;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 16;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 1;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints(tbTable0, gbcPanel0);
        pnPanel0.add(tbTable0);

        return pnPanel0;
    }
}
