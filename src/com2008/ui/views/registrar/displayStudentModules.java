package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class displayStudentModules extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    HashMap props;
    HashMap id;
    public displayStudentModules(Manager mger, HashMap p, HashMap t) {
        super("Display student Modules", new ArrayList<>());
        this.name = "Display Students Modules";
        manager = mger;
        props = p;
        id = t;
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTable tbTable0;
        JButton btBut4;
        JButton btBut5;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All Student Modules" ) );

        pnPanel0.setLayout( new BoxLayout(pnPanel0, BoxLayout.PAGE_AXIS) );

        String[] columnNames = new String[props.size()];

        String[][] data = new String[ props.size()][((ArrayList<String>) props.get("id")).size()];
        int col = 0;
        for ( Object key : props.keySet()) {
            if(key.equals("0")){ continue;};
            List<String> val = (ArrayList<String>) props.get(key);
            columnNames[col] = (String) key;
            System.out.println(val);
            int valSize = val.size();
            String[] valArray = new String[data[col].length];
            valArray = val.toArray(valArray);

            for (int i = 0; i < valSize; i ++) {
                // i is the index
                data[col][i] = valArray[i];//,
            }
            col +=1;
        }



        tbTable0 = new JTable(  transpose(data), columnNames  );
        pnPanel0.add( tbTable0 );

        btBut4 = new JButton( "Add module"  );
        System.out.println("DISPLAY STUDENT MODULE PROPS");
        System.out.println(props);
        btBut4.addActionListener(new change(manager,"selectModuleToAdd",props, id));


        pnPanel0.add( btBut4 );

        btBut5 = new JButton( "Remove module"  );
        btBut5.addActionListener(new change(manager,"removeModuleForm",props, id));

        pnPanel0.add( btBut5 );

        return pnPanel0;
    }

    private class change extends ChangeViewListener {
        HashMap id;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public change(Manager m, String view, HashMap d, HashMap t) {
            super(m,view,d);
            id = t;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            data.put("id",id.get("id"));
            manager.changeView(view,data);
        }
    }

}

