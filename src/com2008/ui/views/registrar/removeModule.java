package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class removeModule extends View implements ViewInterface {
    String name;
    HashMap data;
    public Manager manager;
    public removeModule(Manager mger, HashMap p) {
        super("Remove Module", new ArrayList<>());
        this.name = "Remove a Module";
        manager = mger;
        data = p;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JButton btBut3;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Enter Module ID to be removed" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( gbPanel0 );

        JComboBox module = new JComboBox();
        module.addItem(new ComboItem("Please Select a Module","0"));

        Integer userLength = ((ArrayList<Map<String,String>>) this.data.get("modules")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.data.get("modules")).get(i);
                String label = user.get("ref_code")+" : "+user.get("module_name");
                String userID = user.get("id");
                module.addItem(new ComboItem(label,userID));
            }
        }
        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 4;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( module, gbcPanel0 );
        pnPanel0.add( module );

        btBut3 = new JButton( "Submit"  );
        btBut3.addActionListener(new listener(manager,"removeModule",data, module));
        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 12;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 5;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( btBut3, gbcPanel0 );
        pnPanel0.add( btBut3 );


        return pnPanel0;
    }

    private class listener extends ChangeViewListener {
        JComboBox text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox text) {
            super(m,view,d);
            this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = text.getSelectedItem();
            data.put("module",((ComboItem)item).getValue() );
            manager.changeView(view,data);
        }
    }
}
