package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


public class mainMenu extends View implements ViewInterface{
    String name;
    public Manager manager;
    public mainMenu(Manager mger) {
        super("Main Menu", new ArrayList<>());
        manager = mger;

    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Registrar Menu" ) );

        GridLayout gbPanel0 = new GridLayout(2,2);
        pnPanel0.setLayout( gbPanel0 );

        JButton btBut1 = new JButton("Edit a students optional modules");
        JButton btBut2 = new JButton("Add/remove a student");
        JButton btBut3 = new JButton("Check a student has registered");
        JButton btBut4 = new JButton("Check a students credit sum");
        pnPanel0.add(btBut1);
        pnPanel0.add(btBut2);
        pnPanel0.add(btBut3);
        pnPanel0.add(btBut4);



        //Example of creating how we initialise our listener for pressy.
        HashMap d = new HashMap<>();
        d.put("id","1");
        d.put("ids", new String[]{"1","2"});

        btBut1.addActionListener(new modules(manager,"selectStudentModule",d));
        btBut2.addActionListener(new students(manager,"displayStudents",d));
        btBut3.addActionListener(new registration(manager,"checkRegistered",d));
        btBut4.addActionListener(new credits(manager,"checkCreditSum",d));
        return pnPanel0;
    }
    private class modules extends ChangeViewListener{
        JTextField text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public modules(Manager m, String view, HashMap d) {
            super(m,view,d);
            //this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            //data.put("module",text.getText());
            manager.changeView(view,data);
        }
    }

    private class students extends ChangeViewListener{
        JTextField text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public students(Manager m, String view, HashMap d) {
            super(m,view,d);
            //this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            //data.put("module",text.getText());
            manager.changeView(view,data);
        }
    }

    private class registration extends ChangeViewListener{
        JTextField text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public registration(Manager m, String view, HashMap d) {
            super(m,view,d);
            //this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            //data.put("module",text.getText());
            manager.changeView(view,data);
        }
    }

    private class credits extends ChangeViewListener{
        JTextField text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public credits(Manager m, String view, HashMap d) {
            super(m,view,d);
            //this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            //data.put("module",text.getText());
            manager.changeView(view,data);
        }
    }

}



