package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;


public class displayStudents extends View implements ViewInterface {
    String name;
    HashMap props;
    public Manager manager;
    public displayStudents(Manager mger, HashMap p) {
        super("Display student", new ArrayList<>());
        this.name = "Display Students";
        manager = mger;
        props = p;
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }


    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JScrollPane tbTable0;
        JButton btBut4;
        JButton btBut5;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );

        pnPanel0.setLayout( new GridLayout(2,0) );

        if(this.props.get("error") != null){
            JLabel error = new JLabel();
            error.setText("ERROR: "+this.props.get("error"));
            pnPanel0.add(error);
        }

        JPanel buttonsWrap = new JPanel(new GridBagLayout());
        btBut4 = new JButton( "Add student"  );
        btBut4.addActionListener(new ChangeViewListener(manager, "addStudentForm", new HashMap()));

        buttonsWrap.add( btBut4 );

        Map<Integer,Object> a = ( (Map<Integer, Object>) props.get("data"));

        if(a.size() != 0){
            Map<Integer,Map<String,String>> d = (HashMap) props.get("data");
            Map<String,String> firstRow = (Map<String,String>) d.get(d.keySet().toArray()[0]);
            Integer numberOfColumns = firstRow.size();

            //Make an array for our column names
            String[] columnNames = firstRow.keySet().toArray(new String[numberOfColumns]);
            //Set up our table
            String[][] tableData = new String[ ((HashMap) props.get("data")).size()][numberOfColumns];

            int col = 0;
            for ( Object key : d.keySet()) {
                tableData[col] =  ((Map<String,String>) d.get(key)).values().toArray(new String[numberOfColumns]);
                col +=1;
            }

            btBut5 = new JButton( "Remove student"  );
            btBut5.addActionListener(new ChangeViewListener(manager, "selectStudentToRemove", new HashMap()));

            buttonsWrap.add( btBut5 );

            tbTable0 = new JScrollPane(new JTable( tableData, columnNames ));


            pnPanel0.add( buttonsWrap);
            pnPanel0.add( tbTable0);

        }else{
            pnPanel0.add( buttonsWrap);
            pnPanel0.add(new JLabel("No data exists for students (yet!). Use the add module button above to add some students"));
        }




        return pnPanel0;
    }

}
