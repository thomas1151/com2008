package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addStudent extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public addStudent(Manager mger) {
        super("Main Menu", new ArrayList<>());
        this.name = "Check Credit Sum";
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JTextField tutor = new JTextField();
        JTextField user = new JTextField();
        JTextField title = new JTextField();
        JTextField forename = new JTextField();
        JTextField surname = new JTextField();
        JTextField email = new JTextField();
        JTextField password = new JTextField();
        JTextField degree = new JTextField();

        JButton btBut4;

        JLabel lbLabel1 = new JLabel("Enter Tutor:");
        JLabel lbLabel2 = new JLabel("Enter Username:");
        JLabel lbLabel3 = new JLabel("Enter Title:");
        JLabel lbLabel4 = new JLabel("Enter Forename:");
        JLabel lbLabel5 = new JLabel("Enter Surname:");
        JLabel lbLabel6 = new JLabel("Enter Email:");
        JLabel lbLabel7 = new JLabel("Enter Password:");
        JLabel lbLabel8 = new JLabel("Enter Degree:");



        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));

        contentPane.add(lbLabel1);
        contentPane.add(tutor);
        contentPane.add(lbLabel2);
        contentPane.add(user);
        contentPane.add(lbLabel3);
        contentPane.add(title);
        contentPane.add(lbLabel4);
        contentPane.add(forename);
        contentPane.add(lbLabel5);
        contentPane.add(surname);
        contentPane.add(lbLabel6);
        contentPane.add(email);
        contentPane.add(lbLabel7);
        contentPane.add(password);
        contentPane.add(lbLabel8);
        contentPane.add(degree);

        HashMap p = new HashMap();

        btBut4 = new JButton( "Add student"  );
        btBut4.addActionListener(new listener(manager, "addStudentToDB", p, tutor, user, title, forename, surname, email, password, degree));
        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener {
        JTextField tutor;
        JTextField user;
        JTextField title;
        JTextField forename;
        JTextField surname;
        JTextField email;
        JTextField password;
        JTextField degree;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JTextField tutor, JTextField user, JTextField title, JTextField forename, JTextField surname, JTextField email, JTextField password, JTextField degree) {
            super(m,view,d);
            this.tutor = tutor;
            this.user = user;
            this.title = title;
            this.forename = forename;
            this.surname = surname;
            this.email = email;
            this.password = password;
            this.degree = degree;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            data.put("tutor",tutor.getText());
            data.put("username",user.getText());
            data.put("title",title.getText());
            data.put("forename",forename.getText());
            data.put("surname",surname.getText());
            data.put("email",email.getText());
            data.put("password",password.getText());
            data.put("degree",degree.getText());

            manager.changeView(view,data);
        }
    }
}

