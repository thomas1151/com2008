package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.components.ComboItem;
import com2008.ui.events.ChangeViewListener;
import com2008.ui.events.registar.AddModuleListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class addModule extends View implements ViewInterface {
    String name;
    HashMap props;
    public Manager manager;
    HashMap data;
    public addModule(Manager mger, HashMap props) {
        super("Add module", new ArrayList<>());
        this.name = "Add module";
        this.data = props;
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JComboBox module = new JComboBox();
        module.addItem(new ComboItem("Please Select a Module","0"));

        Integer userLength = ((ArrayList<Map<String,String>>) this.data.get("modules")).size();
        if(userLength > 0){
            for(int i =0; i <  userLength; i++){
                Map<String,String> user = ((ArrayList<Map<String,String>>) this.data.get("modules")).get(i);
                String label = user.get("ref_code")+" : "+user.get("name");
                String userID = user.get("id");
                module.addItem(new ComboItem(label,userID));
            }
        }

//
//        Map<Integer,Map<String,String>> d = (HashMap) this.data.get("data");
//        Integer userLength = d.size();
//        if(userLength > 0){
//
//            for ( Object key : d.keySet()) {
//
////                data[col] =  ((Map<String,String>) d.get(key)).values().toArray(new String[numberOfColumns]);
//                String label = d.get(key).get("ref_code")+" : "+d.get(key).get("module_name");
//                String userID = d.get(key).get("id");
//                module.addItem(new ComboItem(label,userID));
//            }
//
//
//        }

        JButton btBut4;
        JLabel lbLabel8 = new JLabel("Enter module id:");

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0,1));
        contentPane.setBorder(BorderFactory.createTitledBorder("Add Student"));


        contentPane.add(lbLabel8);
        contentPane.add(module);

        btBut4 = new JButton( "Add student"  );
        btBut4.addActionListener(new listener(manager, "addModuleToDatabase", this.data, module));

        contentPane.add( btBut4 );

        return contentPane;
    }

    private class listener extends ChangeViewListener{
        JComboBox text;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public listener(Manager m, String view, HashMap d, JComboBox text) {
            super(m,view,d);
            this.text = text;
        }

        @Override
        public void actionPerformed(ActionEvent a) {
            Object item = text.getSelectedItem();
            data.put("module", ((ComboItem)item).getValue());
            manager.changeView(view,data);
        }
    }

}

