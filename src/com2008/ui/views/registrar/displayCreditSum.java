package com2008.ui.views.registrar;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class displayCreditSum extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data;
    public Manager manager;
    public displayCreditSum(Manager mger, ArrayList<Map<String,String>> data) {
        super("Display Credit Sum", new ArrayList<>() );
        this.name = "Display Credit Sum";
        this.data = data;
        manager = mger;
    }

    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JLabel lbLabel0;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "Display Credit Sum" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( gbPanel0 );


        if(this.data.size() > 0){
            lbLabel0 = new JLabel( "The credit total for this student is: " + this.data.get(0).get("credits") );

        } else{
            lbLabel0 = new JLabel( "No data found" );

        }
        gbcPanel0.gridx = 3;
        gbcPanel0.gridy = 3;
        gbcPanel0.gridwidth = 14;
        gbcPanel0.gridheight = 5;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 1;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( lbLabel0, gbcPanel0 );
        pnPanel0.add( lbLabel0 );

        return pnPanel0;
    }
}


