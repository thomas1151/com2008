
package com2008.ui.views.generic;

import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;


public class ChangePassword extends View implements ViewInterface {
    String name;
    JTextField loginField;
    JPasswordField passField;
    JPasswordField newPassField;
    JPanel panel;
    Manager manager;

    HashMap data;
    public ChangePassword(Manager m, HashMap data) {
        super("Change Password", ViewInterface.HashMapToArrayList(data));
        this.name = "Change Your Password";
        this.manager = m;
        this.data = data;

    }

    @Override
    public JPanel getSelf() {

        panel = new JPanel();
        panel.setLayout( new BoxLayout(panel,1) );


        JLabel login_label = new JLabel("Username");
        login_label.setForeground(Color.BLACK);
        login_label.setBounds(450,115,200,30);
        JLabel pass_label = new JLabel("Existing Password");
        pass_label.setForeground(Color.BLACK);
        JLabel newPassLabel = new JLabel("New Password");
        pass_label.setForeground(Color.BLACK);


        loginField = new JTextField(40);


        passField = new JPasswordField(40);
        newPassField = new JPasswordField(40);
        JButton submitButton = new JButton("Change Pass");

        loginField.setMaximumSize(new Dimension(600,50));
        passField.setMaximumSize(new Dimension(600,50));
        newPassField.setMaximumSize(new Dimension(600,50));

        submitButton.addActionListener(new ChangePassword.LoginListener(manager,"mainMenu",new HashMap(),loginField,passField, newPassField));


        panel.add(login_label, ViewInterface.getSidebarConstraints());
        panel.add(loginField, ViewInterface.getSidebarConstraints());
        panel.add(pass_label, ViewInterface.getSidebarConstraints());
        panel.add(passField, ViewInterface.getSidebarConstraints());
        panel.add(newPassLabel, ViewInterface.getSidebarConstraints());
        panel.add(newPassField, ViewInterface.getSidebarConstraints());
        panel.add(submitButton, ViewInterface.getSidebarConstraints());

        setTitle("Change your password");

        return panel;
    }
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ViewInterface.createAndShowGUI(new ChangePassword(null,null));
            }
        });
    }

    private class LoginListener extends ChangeViewListener{
        JTextField username;
        JPasswordField pass;
        JPasswordField newPass;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public LoginListener(Manager m, String view, HashMap d, JTextField username, JPasswordField pass, JPasswordField newPass) {
            super(m,view,d);
            this.username = username;
            this.pass     = pass;
            this.newPass = newPass;
        }

        @Override
        public void actionPerformed(ActionEvent a) {


            manager.changePassword(username.getText(),new String(pass.getPassword()),new String(newPass.getPassword()));

        }
    }


}

