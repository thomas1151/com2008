package com2008.ui.views.generic;

import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;


public class Logout extends View implements ViewInterface {
    String name;
    JTextField loginField;
    JPasswordField passField;
    JPanel panel;
    Manager manager;

    HashMap data;
    public Logout(Manager m, HashMap data) {
        super("Logout", ViewInterface.HashMapToArrayList(data));
        this.name = "Logout";
        this.manager = m;
        this.data = data;
    }

    @Override
    public JPanel getSelf() {

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        JLabel description = new JLabel("Are you sure you want to log out?");

        JButton submitButton = new JButton("Yes");
        submitButton.addActionListener(new Logout.LogoutListener(manager,"mainMenu",new HashMap()));

        JButton cancelButton = new JButton("Take me back to the menu");
        cancelButton.addActionListener(new ChangeViewListener(manager,"displayMainMenu", new HashMap()));


        if(data.get("failure") != null && (boolean) data.get("failure")){
            panel.add( new JLabel("Please enter valid credentials"));

        }
        panel.add(description, ViewInterface.getSidebarConstraints());
        panel.add(submitButton, ViewInterface.getSidebarConstraints());
        panel.add(cancelButton, ViewInterface.getSidebarConstraints());

        setTitle("Login");

        return panel;
    }
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ViewInterface.createAndShowGUI(new Logout(null,null));
            }
        });
    }

    private class LogoutListener extends ChangeViewListener{

        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public LogoutListener(Manager m, String view, HashMap d) {
            super(m,view,d);

        }

        @Override
        public void actionPerformed(ActionEvent a) {
            manager.logout();

        }
    }


}

