
package com2008.ui.views.generic;

import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static javax.swing.LayoutStyle.ComponentPlacement.UNRELATED;



public class Login extends View implements ViewInterface {
    String name;
    JTextField loginField;
    JPasswordField passField;
    JPanel panel;
    Manager manager;

    HashMap data;
    public Login(Manager m, HashMap data) {
        super("Login", ViewInterface.HashMapToArrayList(data));
        this.name = "Login";
        this.manager = m;
        this.data = data;

    }

    @Override
    public JPanel getSelf() {

        panel = new JPanel();
        panel.setLayout(null);





        JLabel login_label = new JLabel("Username");
        login_label.setForeground(Color.BLACK);
        login_label.setBounds(450,115,200,30);
        JLabel pass_label = new JLabel("Password");
        pass_label.setForeground(Color.BLACK);
        pass_label.setBounds(450,215,200,30);





        loginField = new JTextField(40);
        loginField.setBounds(350,150,300,40);
        passField = new JPasswordField(40);
        passField.setBounds(350,250,300,40);
        JButton submitButton = new JButton("Login");
        submitButton.setBounds(350,330,300,40);

        submitButton.addActionListener(new Login.LoginListener(manager,"mainMenu",new HashMap(),loginField,passField));

        if(data.get("failure") != null && (boolean) data.get("failure")){
            panel.add( new JLabel("Please enter valid credentials"));

        }

        if(data.get("loggedOut") != null && (boolean) data.get("loggedOut")){
            panel.add( new JLabel("Successfully logged out"));

        }
        panel.add(login_label, ViewInterface.getSidebarConstraints());
        panel.add(loginField, ViewInterface.getSidebarConstraints());
        panel.add(pass_label, ViewInterface.getSidebarConstraints());
        panel.add(passField, ViewInterface.getSidebarConstraints());
        panel.add(submitButton, ViewInterface.getSidebarConstraints());

        setTitle("Login");


        return panel;
    }
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                ViewInterface.createAndShowGUI(new com2008.ui.views.generic.Login(null,null));
            }
        });
    }

    private class LoginListener extends ChangeViewListener{
        JTextField username;
        JPasswordField pass;
        /**
         * Constructor
         * @param m The Manager that is controlling our UI.
         * @param view The view (as string) we want to change to.
         */
        public LoginListener(Manager m, String view, HashMap d, JTextField username, JPasswordField pass) {
            super(m,view,d);
            this.username = username;
            this.pass     = pass;
        }

        @Override
        public void actionPerformed(ActionEvent a) {

            System.out.println(username.getText());
            System.out.println(new String(pass.getPassword()));

            manager.login(username.getText(),new String(pass.getPassword()));

        }
    }


}

