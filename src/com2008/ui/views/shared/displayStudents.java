package com2008.ui.views.shared;
import com2008.Manager;
import com2008.ui.View;
import com2008.ui.ViewInterface;
import com2008.ui.events.ChangeViewListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;


public class displayStudents extends View implements ViewInterface {
    String name;
    ArrayList<Map<String,String>> data = new ArrayList<>();
    HashMap props;
    public Manager manager;
    public displayStudents(Manager mger, HashMap p) {
        super("Display student", new ArrayList<>());
        this.name = "Display Students";
        manager = mger;
        props = p;
    }

    public String[][] transpose (String[][] array) {
        if (array == null || array.length == 0)//empty or unset array, nothing do to here
            return array;

        int width = array.length;
        int height = array[0].length;

        String[][] array_new = new String[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array_new[y][x] = array[x][y];
            }
        }
        return array_new;
    }


    @Override
    public JPanel getSelf() {
        JPanel pnPanel0;
        JScrollPane tbTable0;
        JButton btBut4;
        JButton btBut5;

        pnPanel0 = new JPanel();
        pnPanel0.setBorder( BorderFactory.createTitledBorder( "All students" ) );
        GridBagLayout gbPanel0 = new GridBagLayout();
        GridBagConstraints gbcPanel0 = new GridBagConstraints();
        pnPanel0.setLayout( gbPanel0 );

        String[] columnNames = new String[props.size()];

        System.out.println(props);
        System.out.println(props.get("forename"));
        System.out.println(props.get("forename").getClass());

        String[][] data = new String[ props.size()][((ArrayList<String>) props.get("id")).size()];
        int col = 0;
        for ( Object key : props.keySet()) {

            List<String> val = (ArrayList<String>) props.get(key);
            columnNames[col] = (String) key;
            System.out.println(val);
            int valSize = val.size();
            String[] valArray = new String[data[col].length];
            valArray = val.toArray(valArray);

            for (int i = 0; i < valSize; i ++) {
                // i is the index
                data[col][i] = valArray[i];//,
            }
            col +=1;
        }

        System.out.println(Arrays.deepToString(data));
        System.out.println(data);

        tbTable0 = new JScrollPane(new JTable( transpose(data), columnNames ));
        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 2;
        gbcPanel0.gridwidth = 16;
        gbcPanel0.gridheight = 16;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 1;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( tbTable0, gbcPanel0 );
        pnPanel0.add( tbTable0 );

        btBut4 = new JButton( "Add student"  );

        btBut4.addActionListener(new ChangeViewListener(manager, "addStudentForm", new HashMap()));

        gbcPanel0.gridx = 2;
        gbcPanel0.gridy = 16;
        gbcPanel0.gridwidth = 7;
        gbcPanel0.gridheight = 2;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( btBut4, gbcPanel0 );
        pnPanel0.add( btBut4 );

        btBut5 = new JButton( "Remove student"  );
        btBut4.addActionListener(new ChangeViewListener(manager, "selectStudentToRemove", new HashMap()));

        gbcPanel0.gridx = 11;
        gbcPanel0.gridy = 16;
        gbcPanel0.gridwidth = 7;
        gbcPanel0.gridheight = 2;
        gbcPanel0.fill = GridBagConstraints.BOTH;
        gbcPanel0.weightx = 1;
        gbcPanel0.weighty = 0;
        gbcPanel0.anchor = GridBagConstraints.NORTH;
        gbPanel0.setConstraints( btBut5, gbcPanel0 );
        pnPanel0.add( btBut5 );


        return pnPanel0;
    }

}
