package com2008.ui.components;
import javax.swing.*;
import java.awt.*;

/**
 * Our custom menu button, which has specific background and passed text.
 */
public class MenuButton extends JButton {
    public MenuButton(String text) {
        //Set background
        //setBackground(new Color(220, 220, 220));
        //Set the text
        setText(text);
        setPreferredSize(new Dimension(300,70));
        setBackground(new Color(97,154,194));
        setForeground(Color.pink);
        setFont(new Font("Broadway BT",Font.PLAIN,16));
    }
}




