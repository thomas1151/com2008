/**
 * https://stackoverflow.com/questions/17887927/adding-items-to-a-jcombobox
 */

package com2008.ui.components;

public class ComboItem {
    private String key;
    private String value;

    public ComboItem(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString()
    {
        return key;
    }

    public String getKey()
    {
        return key;
    }

    public String getValue()
    {
        return value;
    }
}
