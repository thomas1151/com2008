package DbManage;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.dbutils.DbUtils;

public class DBManager {

    //These need to be protected.
    public String url;
    public String user;
    public String password;

    //Make the connection public to child classes
    public Connection conn;

    /**
     * Connection to a DB
     * @param url the url (jdbc:mysql://)
     * @param user username
     * @param password pass
     */
    public DBManager(String url,String user, String password){
        this.url = url;
        this.user = user;
        this.password = password;
        this.conn = null;
    }

    /**
     * helper function for executeQuery/executeStatement that takes a
     * valid MySQL query, whether its an update, and the columns to return,
     * and returns them in the form of a HashMap.
     * @param q      the query to update
     * @param update boolean, is it updatable?
     * @param cols   String[], which columns we want back.
     * @return       ArrayList with key,value tuples.
     */
    public  ArrayList<Map<String,String>> executeToObj(String q, Boolean update, String[] cols){

        //define our props.
        Statement st = null;
        ResultSet rs = null;
        Boolean success = false;
        ArrayList<Map<String,String>> maps = new ArrayList<>();

        try {
            //Try and connect with our database using the parameters sent.
            this.conn = DriverManager.getConnection(this.url, this.user, this.password);

            //Create blank statement
            st = conn.createStatement();
            if(update){
                int rowsAffected = st.executeUpdate(q);
            }else{

                //TODO:Rewrite this so the query takes the column desired
                rs = st.executeQuery(q);

                ResultSetMetaData resultSetMetaData = rs.getMetaData();
//                Debug function which prints out the cols and the vals
//                int columnsNumber = resultSetMetaData.getColumnCount();
//                while (rs.next()) {
//                    for (int i = 1; i <= columnsNumber; i++) {
//                        if (i > 1) System.out.print(",  ");
//                        String columnValue = rs.getString(i);
//                        System.out.print(columnValue + " " + resultSetMetaData.getColumnLabel(i));
//                    }
//                    System.out.println("");
//                }
//                rs.beforeFirst();

                //While we have data left to check.
                while (rs.next()) {
                    //Let's create our hashmap.
                    Map<String, String> row = new HashMap<>();
                    //We're going to get the names of the columns, and use this to add to the hashmap.
                    for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                        //Lazy check here to see if we need to filter our columns.
                        if(cols.length == 0 ){
                            row.put(resultSetMetaData.getColumnLabel(i), rs.getString(i));
                        }else{
                            for (int l = 0; l < cols.length; l++) {
                                if(resultSetMetaData.getColumnLabel(i).equals(cols[l])){
                                    row.put(resultSetMetaData.getColumnLabel(i), rs.getString(i));
                                }
                            }
                        }

                    }
                    maps.add(row);

                }
                //Nice little output here.

                System.out.println("Database successfully queried");
            }


        } catch (SQLException ex) {
            //Not successful :(
            Logger lgr = Logger.getLogger(DBManager.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            //Close the connections either way.
            if (this.conn != null) {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(conn);
            }
        }

        //Return the mapping.
        return maps;
    }

    public boolean executeStatement(String q, Boolean update){
        Statement st = null;
        ResultSet rs = null;
        Boolean success = false;
        try {
            this.conn = DriverManager.getConnection(this.url, this.user, this.password);
            st = conn.createStatement();
            if(update){
                int rowsAffected = st.executeUpdate(q);

            }else{
                rs = st.executeQuery(q);
                while(rs.next()){
                    System.out.println(rs.getString(1));
                }

                System.out.println("Database successfully queried");
            }
            success = true;

        } catch (SQLException ex) {

            Logger lgr = Logger.getLogger(DBManager.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            if (this.conn != null) {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(conn);
            }
        }
        return success;
    }

    /**
     * Executes an update on the DB.
     * @param q Query
     * @return success or no
     */
    public boolean executeUpdate(String q){
        Boolean update = true;
        return executeStatement(q, update);
    }

    /**
     * Executes a query, returns array list of the cols we want
     * @param q     Query
     * @param cols  The columns we want
     * @return result (empty if none)
     */
    public ArrayList<Map<String,String>> executeQuery(String q, String[] cols){
        Boolean update = false;
        return executeToObj(q, update, cols );

    }

    /**
     * Creates a new table in the DB.
     * @param tableName name of table.
     * @param cols      the columns in the DB.
     * @return success or no.
     */
    public boolean createTable(String tableName,String[][] cols){
        String query = "CREATE TABLE "+tableName+" "+cols;
        System.out.println(query);
        return executeUpdate(query);

    }

}