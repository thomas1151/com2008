package DbManage;

import com2008.controllers.RegistrarController;
import com2008.routes.jbcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

public class RegistrationManager extends DBManager {

    public RegistrationManager(String url,String user, String password){
        super(url, user, password);
    }

    public ArrayList<Map<String,String>> getDepartments() {
        String[] cols = new String[]{};
        return this.executeQuery("SELECT * FROM Department;", cols);
    }

    public Integer getPrivilege(String username, String password){
        if(checkPasswordEquality(username, password)){
            return Integer.parseInt(this.executeQuery("SELECT role FROM User where username=\""+username+" \";", new String[]{"role"}).get(0).get("role"));
        }
        return -1;
    }

    public ArrayList<Map<String,String>> getStudentInfo(int id, String password) {
        String[] cols = new String[]{};
        return this.executeToObj("SELECT overall_grade, studyperiod, level "
                + "FROM (StudentStudyPeriod ssp "
                + "INNER JOIN Student s ON s.id = ssd.student "
                + "WHERE s.id ="+id+", s.password="+password+";", false, cols );
    }


    private String genHash(String toHash){
        // Check that an unencrypted password matches one that has
        // previously been hashed
        return BCrypt.hashpw(toHash, BCrypt.gensalt());
    }
    public boolean generatePassword(String pass,Integer user){
        // Hash a password for the first time
       executeUpdate("UPDATE User"+
                                " SET pass=\""+genHash(pass)+"\""+
                                " WHERE User.id="+user+";");
        return true;
    }
    public boolean checkPasswordEquality(String user, String pass){
        String q  = "SELECT pass FROM User WHERE User.username=\""+user+"\";";
        ArrayList<Map<String,String>> d = executeQuery(q,new String[]{"pass"});
        if(d.size() > 0){
            if(BCrypt.checkpw(pass,d.get(0).get("pass"))){
                return true;
            }
        }
        return false;
    }
    public boolean checkValidity(){
        return true;
    }

    public static void main(String[] args) {
        RegistrationManager db = new RegistrationManager("jdbc:mysql://stusql.dcs.shef.ac.uk/team011","team011","2fcf69f0");
//        RegistrationManager db = new RegistrationManager("jdbc:mysql://192.168.1.69/com2008","team011","2fcf69f0");

        System.out.println(db.getDepartments());

    }
}
